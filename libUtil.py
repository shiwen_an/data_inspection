# Author: Shiwen An
# Date: 2021/11/30
# Purpose: Build a library for 
#        All The Utility Function 


# Author: Shiwen An 
# Date: 2021/11/28
# Purpose: Storing the example code for the physics analysis
#          Somehow might be useful in the future

from __future__ import print_function
from ROOT import TCanvas, TGraph
from ROOT import gROOT
from math import sin
from array import array

import ROOT as root
import atlasplots as aplt
import matplotlib.pyplot as plt
import numpy as np

# ATLAS Python Format
# Nice library helps people in the group out
# https://atlas-plots.readthedocs.io/en/latest/examples.html
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

import seaborn as sns

# sns.lineplot(
#     x="a", y="b",
#     size="coherence", hue="choice",
#     legend="full"
# )

def readfile(filename):
    f= open(filename, "r")
    a = []
    for x in f: 
        x = x[0: len(x)-1]
        a.append(x)
    a = sorted(a)
    a = sorted(a, key=len)
    # print(a)
    return a 

# Heat map with Hit number and region
def histo_2d_number(map, x, y, title, save_name):
    fig, ax = plt.subplots(figsize=(20,25))
    # ax.set(xticks = np.arange(x), yticks=np.arange(y))
    im = ax.imshow(map, cmap='gray')

    for i in range(x):
        for j in range(y):
            text = ax.text(j, i, int(map[j,i]), 
                ha ="center", va = "center", color = "w" )
    ax.set_title(title)
    plt.show()
    fig.savefig(save_name)

def graph_1d_simple(x, y, xl, yl, filename, title):
    fig1 = plt.gcf()
    for y1 in y:
        plt.plot(x, y1)

    plt.xlabel(xl)
    plt.ylabel(yl)
    plt.title(title)
    plt.grid(True)
    #plt.show()
    fig1.savefig(filename)
    plt.clf()


def graph_1d(x, y, filename, xlabel, ylabel, title, legend):

    aplt.set_atlas_style()
    fig, ax = aplt.subplots(1, 1, name="fig1", figsize=(800, 600))

    graph = ax.graph(x,y, label = legend, figsize=(800,600))
    #graph.Fit("pol3", "0", "1","2") # should be some linear fit
    ax.add_margins(top=0.18, left=0.05, right=0.05, bottom=0.05)

    #Fitting added based on individual needs
    #func = graph.GetFunction("pol3")
    #func.SetRange(*ax.get_xlim())
    #func.SetNpx(1000)
    #ax.plot(func, linecolor=root.kRed+1, expand=False, label="Fit", labelfmt="L")

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    aplt.atlas_label(text="ITk pixel Internal", loc="upper left")
    ax.text(0.2, 0.84, title, size=22, align=13)

    ax.legend(loc=(0.65, 0.8, 0.95, 0.92))
    fig.savefig(filename)

# Three different value imitate the atlas plot
def avg_std_graph(start, bin, avg,std, pdf_name, xlabel, ylabel):
    x = []
    y = []
    aplt.set_atlas_style()
    fig, ax = aplt.subplots(1, 1, name="fig1", figsize=(800, 600))
    for i,v in enumerate(avg):
        x.append(i*bin+start)
        y.append(v)
    yerr = std
    graph = ax.graph(x,y, yerr = yerr, label = "Avg", figsize=(800,600))
    graph.Fit("pol1", "0") # should be some linear fit
    ax.add_margins(top=0.18, left=0.05, right=0.05, bottom=0.05)

    func = graph.GetFunction("pol1")
    func.SetRange(*ax.get_xlim())
    func.SetNpx(1000)
    ax.plot(func, linecolor=root.kRed+1, expand=False, label="Fit", labelfmt="L")

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    aplt.atlas_label(text="ITk pixel Internal", loc="upper left")
    ax.text(0.2, 0.84, "KEKQ13 Chip3 4.6A, -100V", size=22, align=13)

    ax.legend(loc=(0.65, 0.8, 0.95, 0.92))
    fig.savefig(pdf_name)

def histo_2d_s_curve(y_list, xbin, xlow, xhigh, ybin, ylow, yhigh, filename, xlabel, ylabel):
    aplt.set_atlas_style()
    fig, ax = aplt.subplots(1, 1, name="fig1", figsize=(800, 600))

    hist = root.TH2F("hist", "2D Map", xbin, xlow, xhigh, ybin, ylow, yhigh)
    bin_s = (xhigh-xlow)/xbin

    for y in y_list:
        for i, v in enumerate(y):
            a = xlow+i*bin_s
            hist.Fill(a,v)

    # Draw the histogram on these axes
    ax.plot2d(hist, "COLZ")

    # Change pad margins to allow space for z-axis colour bar and for ATLAS label
    ax.set_pad_margins(right=0.20, top=0.08)

    # Set axis titles
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_zlabel("Events", titleoffset=1.2)

    # Add the ATLAS Label
    aplt.atlas_label(ax.pad.GetLeftMargin(), 0.97, text="ITk pixel Internal", align=13)
    ax.text(1 - ax.pad.GetRightMargin(), 0.97, "KEKQ13 4.6A 100V", size=22, align=33)

    # Save the plot as a PDF
    fig.savefig(filename)    

# 2D Distribution for the TDAC with different threshold
def histo_2d_signal(maps, xbin, xlow, xhigh, ybin, ylow, yhigh, filename, xlabel, ylabel):
    aplt.set_atlas_style()
    fig, ax = aplt.subplots(1, 1, name="fig1", figsize=(800, 600))

    hist = root.TH2F("hist", "2D Map", xbin, xlow, xhigh, ybin, ylow, yhigh)
    bin_s = (xhigh-xlow)/xbin

    for i, v in enumerate(maps):
        for m,n in enumerate(v):
            for k in n:
                a = xlow+i*bin_s
                hist.Fill(a,k)

    # Draw the histogram on these axes
    ax.plot2d(hist, "COLZ")

    # Change pad margins to allow space for z-axis colour bar and for ATLAS label
    ax.set_pad_margins(right=0.20, top=0.08)

    # Set axis titles
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_zlabel("Events", titleoffset=1.2)

    # Add the ATLAS Label
    aplt.atlas_label(ax.pad.GetLeftMargin(), 0.97, text="ITk pixel Internal", align=13)
    ax.text(1 - ax.pad.GetRightMargin(), 0.97, "KEKQ13 4.6A 100V", size=22, align=33)

    # Save the plot as a PDF
    fig.savefig(filename)
   
# ratio plot 
def histo_1d_ratio_tdac(maps,xbin, xlow, xhigh, filename, xlabel, ylabel, title):
    aplt.set_atlas_style()
    fig, (ax1, ax2) = aplt.ratio_plot(name="fig1", figsize=(800, 800), hspace=0.05)

    # Define a distribution
    # sqroot = root.TF1("sqroot", "x*gaus(0) + [3]*abs(sin(x)/x)", 0, 10)
    # sqroot.SetParameters(10, 4, 1, 20)

    # Randomly fill two histograms according to the above distribution
    hist1 = root.TH1F("hist1", "Random Histogram 1", xbin, xlow, xhigh)
    hist1.SetFillColor(root.kAzure+1)
    hist2 = root.TH1F("hist2", "Random Histogram 2", xbin, xlow, xhigh)
    bin_s = (xhigh-xlow)/xbin

    for i, v in enumerate(maps):
        a = xlow+i*bin_s
        for m,n in enumerate(v):
            for k in n:
                hist2.Fill(a)
                if k == 15 or k==-15:
                    hist1.Fill(a)

    # Draw the histograms on these axes
    err_band = aplt.root_helpers.hist_to_graph( hist1, show_bin_width =True)
    ax1.plot(err_band,"2",fillcolor=1, fillstyle= 3254)
    ax1.plot(hist1,label= "TDAC")
    #ax1.plot(hist2, linecolor=root.kBlue+1, label="Blue", labelfmt="L")

    # Draw line at y=1 in ratio panel
    line = root.TLine(ax1.get_xlim()[0], 0.003, ax1.get_xlim()[1], 0.003)
    line1 = root.TLine(ax1.get_xlim()[0], 0.01, ax1.get_xlim()[1], 0.01)
    ax2.plot(line)
    ax2.plot(line1)

    # Calculate and draw the ratio
    ratio_hist = hist1.Clone("ratio_hist")
    ratio_hist.Divide(hist2)

    ax2.plot(ratio_hist, "P")

    # Add extra space at top of plot to make room for labels
    ax1.add_margins(top=0.16)

    # Set axis titles
    ax2.set_xlabel(xlabel)
    ax1.set_ylabel(ylabel)
    ax2.set_ylabel("Percent.", loc="centre")

    # Add extra space at top and bottom of ratio panel
    ax2.add_margins(top=0.1, bottom=0.1)

    # Go back to top axes to add labels
    ax1.cd()

    # Add the ATLAS Label
    aplt.atlas_label(text="ITk pixel Internal", loc="upper left")
    ax1.text(0.2, 0.84, "KEKQ13 Chip3 4.6A -100V", size=22, align=13)

    # Add legend
    ax1.legend(loc=(0.78, 0.78, 1, 0.90))

    # Save the plot as a PDF
    fig.savefig(filename)

def histo_1d_ratio_threshold(maps,xbin, xlow, xhigh, filename, xlabel, ylabel, title):
    aplt.set_atlas_style()
    fig, (ax1, ax2) = aplt.ratio_plot(name="fig1", figsize=(800, 800), hspace=0.05)

    # Define a distribution
    # sqroot = root.TF1("sqroot", "x*gaus(0) + [3]*abs(sin(x)/x)", 0, 10)
    # sqroot.SetParameters(10, 4, 1, 20)

    # Randomly fill two histograms according to the above distribution
    hist1 = root.TH1F("hist1", "Random Histogram 1", xbin, xlow, xhigh)
    hist1.SetFillColor(root.kRed+1)
    hist2 = root.TH1F("hist2", "Random Histogram 2", xbin, xlow, xhigh)
    bin_s = (xhigh-xlow)/xbin
    print("xlow", xlow)
    print("xhigh", xhigh)
    print(bin_s)

    for i, v in enumerate(maps):
        a = xlow+i*bin_s
        print(a)
        for m,n in enumerate(v):
            for k in n:
                hist2.Fill(a)
                if k == 1:
                    
                    hist1.Fill(a)

    # Draw the histograms on these axes
    err_band = aplt.root_helpers.hist_to_graph( hist1, show_bin_width =True)
    ax1.plot(err_band,"2",fillcolor=1, fillstyle= 3254)
    hist1_graph = aplt.root_helpers.hist_to_graph( hist1)
    ax1.plot(hist1, label=title)
    #ax1.plot(hist2, linecolor=root.kBlue+1, label="Blue", labelfmt="L")

    # Draw line at y=1 in ratio panel
    line = root.TLine(ax1.get_xlim()[0], 0.003, ax1.get_xlim()[1], 0.003)
    line1 = root.TLine(ax1.get_xlim()[0], 0.01, ax1.get_xlim()[1], 0.01)
    ax2.plot(line)
    ax2.plot(line1)

    # Calculate and draw the ratio
    ratio_hist = hist1.Clone("ratio_hist")
    ratio_hist.Divide(hist2)

    ax2.plot(ratio_hist, "P")

    # Add extra space at top of plot to make room for labels
    ax1.add_margins(top=0.16)

    # Set axis titles
    ax2.set_xlabel(xlabel)
    ax1.set_ylabel(ylabel)
    ax2.set_ylabel("Percent.", loc="centre")

    # Add extra space at top and bottom of ratio panel
    ax2.add_margins(top=0.1, bottom=0.1)

    # Go back to top axes to add labels
    ax1.cd()

    # Add the ATLAS Label
    aplt.atlas_label(text="ITk Pixel Internal", loc="upper left")
    ax1.text(0.2, 0.84, "KEKQ13 Chip3 4.6A -100V", size=22, align=13)

    # Add legend
    ax1.legend(loc=(0.78, 0.78, 1, 0.90))

    # Save the plot as a PDF
    fig.savefig(filename)

def histo_1d_ratio_threshold_avg_line(maps,xbin, xlow, xhigh, filename, xlabel, ylabel, title, txt):
    aplt.set_atlas_style()
    fig, (ax1, ax2) = aplt.ratio_plot(name="fig1", figsize=(800, 800), hspace=0.05)

    # Define a distribution
    # sqroot = root.TF1("sqroot", "x*gaus(0) + [3]*abs(sin(x)/x)", 0, 10)
    # sqroot.SetParameters(10, 4, 1, 20)

    # Randomly fill two histograms according to the above distribution
    hist1 = root.TH1F("hist1", "Random Histogram 1", xbin, xlow, xhigh)
    hist1.SetFillColor(root.kRed+1)
    hist2 = root.TH1F("hist2", "Random Histogram 2", xbin, xlow, xhigh)
    bin_s = (xhigh-xlow)/xbin
    print("xlow", xlow)
    print("xhigh", xhigh)
    print(bin_s)

    cnt = []
    cnt_ = 0
    for i, v in enumerate(maps):
        a = xlow+i*bin_s
        print(a)
        for m,n in enumerate(v):
            for k in n:
                hist2.Fill(a)
                if k == 1:
                    hist1.Fill(a)
                    cnt_ = cnt_ +1
        cnt.append(cnt_)
        cnt_ = 0
    avg = np.mean(cnt)

    # Draw the histograms on these axes
    err_band = aplt.root_helpers.hist_to_graph( hist1, show_bin_width =True)
    ax1.plot(err_band,"2",fillcolor=1, fillstyle= 3254)
    hist1_graph = aplt.root_helpers.hist_to_graph( hist1)
    ax1.plot(hist1, label=title)
    #ax1.plot(hist2, linecolor=root.kBlue+1, label="Blue", labelfmt="L")
    line_avg = root.TLine(ax1.get_xlim()[0], avg, ax1.get_xlim()[1], avg)
    ax1.plot(line_avg)

    # Draw line at y=1 in ratio panel
    line = root.TLine(ax1.get_xlim()[0], 0.003, ax1.get_xlim()[1], 0.003)
    line1 = root.TLine(ax1.get_xlim()[0], 0.01, ax1.get_xlim()[1], 0.01)
    ax2.plot(line)
    ax2.plot(line1)

    # Calculate and draw the ratio
    ratio_hist = hist1.Clone("ratio_hist")
    ratio_hist.Divide(hist2)

    ax2.plot(ratio_hist, "P")

    # Add extra space at top of plot to make room for labels
    ax1.add_margins(top=0.16)

    # Set axis titles
    ax2.set_xlabel(xlabel)
    ax1.set_ylabel(ylabel)
    ax2.set_ylabel("Percent.", loc="centre")

    # Add extra space at top and bottom of ratio panel
    ax2.add_margins(top=0.1, bottom=0.1)

    # Go back to top axes to add labels
    ax1.cd()

    # Add the ATLAS Label
    aplt.atlas_label(text="ITk Pixel Internal", loc="upper left")
    ax1.text(0.2, 0.84, txt , size=22, align=13)

    # Add legend
    ax1.legend(loc=(0.78, 0.78, 1, 0.90))

    # Save the plot as a PDF
    fig.savefig(filename)

def histo_1d_simple_0(map, xbin, xlow, xhigh, filename, xlabel, ylabel, title):
    aplt.set_atlas_style()
    fig, ax = aplt.subplots(1, 1, name="fig1", figsize=(800, 600))

    # Activate only when necessary
    # Define a distribution
    # sqroot = root.TF1("sqroot", "x*gaus(0) + [3]*abs(sin(x)/x)", 0, 10)
    # sqroot.SetParameters(10, 4, 1, 20)

    # Randomly fill the histogram according to the above distribution
    hist = root.TH1F("hist", "Simple Histogram", xbin, xlow, xhigh)
    
    for v in map:
        for n in v:
            if n!=1000:
                hist.Fill(n)

    # Fit the histogram with the original distribution; store graphics func but do not draw
    #hist.Fit("sqroot", "0")

    # Draw the histogram on these axes
    ax.plot(hist, label=title, labelfmt="F")

    # Draw the fit function
    #sqroot.SetNpx(1000)
    #ax.plot(sqroot, label="Fit", labelfmt="L", linecolor=root.kRed+1)

    # Add extra space at top of plot to make room for labels
    ax.add_margins(top=0.16)

    # Set axis titles
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    # Add the ATLAS Label
    aplt.atlas_label(text="ITk pixel Internal", loc="upper left")
    ax.text(0.2, 0.86, "KEKQ13 Chip3 4.6A -100V", size=22, align=13)

    # Add legend
    ax.legend(loc=(0.65, 0.8, 0.95, 0.92))

    # Save the plot as a PDF
    fig.savefig(filename)

# Plot distribution of one map only
def histo_1d_simple(map, xbin, xlow, xhigh, filename, xlabel, ylabel, title):
    aplt.set_atlas_style()
    fig, ax = aplt.subplots(1, 1, name="fig1", figsize=(800, 600))

    # Activate only when necessary
    # Define a distribution
    # sqroot = root.TF1("sqroot", "x*gaus(0) + [3]*abs(sin(x)/x)", 0, 10)
    # sqroot.SetParameters(10, 4, 1, 20)

    # Randomly fill the histogram according to the above distribution
    hist = root.TH1F("hist", "Simple Histogram", xbin, xlow, xhigh)
    
    for v in map:
        for n in v:
            hist.Fill(n)

    # Fit the histogram with the original distribution; store graphics func but do not draw
    #hist.Fit("sqroot", "0")

    # Draw the histogram on these axes
    ax.plot(hist, label=title, labelfmt="F")

    # Draw the fit function
    #sqroot.SetNpx(1000)
    #ax.plot(sqroot, label="Fit", labelfmt="L", linecolor=root.kRed+1)

    # Add extra space at top of plot to make room for labels
    ax.add_margins(top=0.16)

    # Set axis titles
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    # Add the ATLAS Label
    aplt.atlas_label(text="ITk pixel Internal", loc="upper left")
    ax.text(0.2, 0.86, "KEKQ13 Chip3 4.6A -100V", size=22, align=13)

    # Add legend
    ax.legend(loc=(0.65, 0.8, 0.95, 0.92))

    # Save the plot as a PDF
    fig.savefig(filename)

def histo_1d_simple_th(map, xbin, xlow, xhigh, filename, xlabel, ylabel, title):
    aplt.set_atlas_style()
    fig, ax = aplt.subplots(1, 1, name="fig1", figsize=(800, 600))

    # Activate only when necessary
    # Define a distribution
    # sqroot = root.TF1("sqroot", "x*gaus(0) + [3]*abs(sin(x)/x)", 0, 10)
    # sqroot.SetParameters(10, 4, 1, 20)

    # Randomly fill the histogram according to the above distribution
    hist = root.TH1F("hist", "Simple Histogram", xbin, xlow, xhigh)
    cnt = 0
    for v in map:
        for n in v:
            if n !=1500 and n!=0:
                hist.Fill(n)
                cnt = cnt+1

    # Fit the histogram with the original distribution; store graphics func but do not draw
    #hist.Fit("sqroot", "0")

    # Draw the histogram on these axes
    ax.plot(hist, label=title, labelfmt="F")

    # Draw the fit function
    #sqroot.SetNpx(1000)
    #ax.plot(sqroot, label="Fit", labelfmt="L", linecolor=root.kRed+1)

    # Add extra space at top of plot to make room for labels
    ax.add_margins(top=0.16)

    # Set axis titles
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    # Add the ATLAS Label
    aplt.atlas_label(text="ITk pixel Internal", loc="upper left")
    ax.text(0.2, 0.86, "KEKQ13 Chip3 4.6A -100V", size=22, align=13)

    # Add legend
    ax.legend(loc=(0.60, 0.85, 0.95, 0.92))

    # Save the plot as a PDF
    print("Histogram Add", cnt)
    fig.savefig(filename)



# Simple histogram plot with multiple plots stack together
def histo_1d_multiple(maps, xbin, xlow, xhigh, filename, xlabel, ylabel, title):
    aplt.set_atlas_style()
    fig, ax = aplt.subplots(1, 1, name="fig1", figsize=(800, 600))

    # Activate only when necessary
    # Define a distribution
    # sqroot = root.TF1("sqroot", "x*gaus(0) + [3]*abs(sin(x)/x)", 0, 10)
    # sqroot.SetParameters(10, 4, 1, 20)

    a = []
    b = ['1000e', '1500e', '2000e'] # Hardcoding not good but please tolerate for now
    c = [root.kBlack, root.kRed+1 , root.kBlue+1 ]
    # Randomly fill the histogram according to the above distribution
    for n, m in enumerate(maps):
        s1 = "hist"+str(n)
        s2 = "Simple histogram"+str(n)
        hist = root.TH1F(s1, s2, xbin, xlow, xhigh)
        for v in m:
            for x in v:
                hist.Fill(x)
        a.append(hist)

    # Fit the histogram with the original distribution; store graphics func but do not draw
    #hist.Fit("sqroot", "0")

    # Draw the histogram on these axes
    for e,h in enumerate(a):
        ax.plot(h, label=b[e], labelfmt="F", linecolor = c[e])

    # Draw the fit function
    #sqroot.SetNpx(1000)
    #ax.plot(sqroot, label="Fit", labelfmt="L", linecolor=root.kRed+1)

    # Add extra space at top of plot to make room for labels
    ax.add_margins(top=0.16)

    # Set axis titles
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    # Add the ATLAS Label
    aplt.atlas_label(text="ITk pixel Internal", loc="upper left")
    ax.text(0.2, 0.86, "KEKQ13 Chip3 4.6A -100V", size=22, align=13)

    # Add legend
    ax.legend(loc=(0.65, 0.8, 0.95, 0.92))

    # Save the plot as a PDF
    fig.savefig(filename)

# Suppose the Prediction of TDAC setup finally should be a
# Gaussian Function
# How is the final TDAC value is going to match the result
def histo_signal(map):

    aplt.set_atlas_style()
    
    # Create a figure and axes
    fig, (ax1, ax2) = aplt.ratio_plot(name="fig1", figsize=(800, 800), hspace=0.05)

    # Define "signal" and data distribution functions
    sig_func = root.TF1("sig_func", "x*gaus(0)",-16,16)
    sig_func.SetParameters(0.1,-1,4) # set parameters for root gaussian

    sig_hist = root.TH1F("sig_hist", "Signal",32,-16,16)
    
    # Fill the map value
    # Here is the core for filling this map
    # other parts are just some style code
    for i,v in enumerate(map):
        for x in v:
            sig_hist.Fill(x)
    
    sig_hist.SetFillColor(root.kAzure+1)
    sig_hist.SetLineWidth(0)
    sig_hist.Sumw2()

    # Fill the Prediction data value
    data_hist = root.TH1F("data_hist", "Data", 32,-16,16)
    data_hist.FillRandom("sig_func",26112)# Hard coding here, TODO change to pixel row*col
    data_hist.Sumw2()
    data_hist.SetBinErrorOption(root.TH1.EBinErrorOpt.kPoisson)  # Use 68% Poisson errors

    # Add Stack to different signal and Background
    # Maybe necessary later when dealing with multiple plots
    bkg_and_sig = root.THStack("bkg_and_sig", "")
    bkg_and_sig.Add(sig_hist)

    ax1.plot(bkg_and_sig)

    # Plot the MC stat error as a hatched band
    # not sure whether add it or not
    err_band = aplt.root_helpers.hist_to_graph(
        bkg_and_sig.GetStack().Last(),
        show_bin_width=True
    )
    ax1.plot(err_band, "2", fillcolor=1, fillstyle=3254, linewidth=0)

    # Plot the data as a graph
    data_graph = aplt.root_helpers.hist_to_graph(data_hist)
    ax1.plot(data_graph, "P")

    # Use same x-range in lower axes as upper axes
    # Draw line at y=1 in ratio panel
    ax2.set_xlim(ax1.get_xlim())
    line = root.TLine(ax1.get_xlim()[0], 1, ax1.get_xlim()[1], 1)
    ax2.plot(line)

    # Plot the relative error on the ratio axes
    err_band_ratio = aplt.root_helpers.hist_to_graph(
        bkg_and_sig.GetStack().Last(),
        show_bin_width=True,
        norm=True
    )
    ax2.plot(err_band_ratio, "2", fillcolor=1, fillstyle=3254)

    # Calculate and draw the ratio
    ratio_hist = data_hist.Clone("ratio_hist")
    ratio_hist.Divide(bkg_and_sig.GetStack().Last())
    ratio_graph = aplt.root_helpers.hist_to_graph(ratio_hist)
    ax2.plot(ratio_graph, "P")

    # Add extra space at top of plot to make room for labels
    ax1.add_margins(top=0.16)

    # Set axis titles
    ax2.set_xlabel("TDAC [-15,15]")
    ax1.set_ylabel("Events")
    ax2.set_ylabel("Events / Pred.", loc="centre")

    ax2.set_ylim(0.55, 1.45)

    # Go back to top axes to add labels
    ax1.cd()

    # Add the ATLAS Label
    aplt.atlas_label(text="ITk pixel Internal", loc="upper left")
    ax1.text(0.2, 0.84, "KEKQ13 4.6A -100V", size=22, align=13)

    # Save the plot as a png
    fig.savefig("tdac_test.png")
 
def histo_1d_signal_tdac(maps):
    aplt.set_atlas_style()
    fig, (ax1, ax2) = aplt.ratio_plot(name="fig1", figsize=(800, 800), hspace=0.05)
    sig_hist = root.TH1F("sig_hist", "TDAC", 55, 1000, 2100)
    data_hist = root.TH1F("data_hist", "TDAC_THRESHOLD", 55, 1000,2100)
    
    # Fill the map value
    # Here is the core for filling this map
    # other parts are just some style code
    for i, v in enumerate(maps):
        a = 1000+i*20
        for m,n in enumerate(v):
            for k in n:
                data_hist.Fill(a)
                if k == 15 or k==-15:
                    sig_hist.Fill(a)
    
    sig_hist.SetFillColor(root.kAzure+1)
    sig_hist.SetLineWidth(0)
    sig_hist.Sumw2()

    # Fill the Error bar for the total TDAC+Threshold
    data_hist.Sumw2()
    data_hist.SetBinErrorOption(root.TH1.EBinErrorOpt.kPoisson)

    # Add Stack to different signal and Background
    # Maybe necessary later when dealing with multiple plots
    bkg_and_sig = root.THStack("bkg_and_sig", "")
    bkg_and_sig.Add(sig_hist)
    ax1.plot(bkg_and_sig)

    # Plot the MC stat error as a hatched band
    # not sure whether add it or not
    err_band = aplt.root_helpers.hist_to_graph(
        bkg_and_sig.GetStack().Last(),
        show_bin_width=True
    )
    ax1.plot(err_band, "2", fillcolor=1, fillstyle=3254, linewidth=0)

    # Use same x-range in lower axes as upper axes
    # Draw line at y=0.05 in ratio panel
    ax2.set_xlim(ax1.get_xlim())
    line = root.TLine(ax1.get_xlim()[0], 0.05, ax1.get_xlim()[1], 0.05)
    ax2.plot(line)

    # Plot the relative error on the ratio axes
    err_band_ratio = aplt.root_helpers.hist_to_graph(
        bkg_and_sig.GetStack().Last(),
        show_bin_width=True,
        norm=True
    )
    ax2.plot(err_band_ratio, "2", fillcolor=1, fillstyle=3254)

    # Calculate and draw the ratio
    ratio_hist = sig_hist.Clone("ratio_hist")
    ratio_hist.Divide(data_hist)
    ratio_graph = aplt.root_helpers.hist_to_graph(ratio_hist)
    ax2.plot(ratio_graph, "P")

    # Add extra space at top of plot to make room for labels
    ax1.add_margins(top=0.16)
    # Legend = root.Tlegend(0.65, 0.78, 0.92, 0.90)
    # Legend.AddEntry(sig_hist, "TDAC", "f")
    # Legend.Draw()

    # Set axis titles
    ax2.set_xlabel("Threshold Tuning [e]")
    ax1.set_ylabel("TDAC +/- 15")
    ax2.set_ylabel("Events / Pixels #", loc="centre")

    ax2.set_ylim(0, 0.1)

    # Go back to top axes to add labels
    ax1.cd()

    # Add the ATLAS Label
    aplt.atlas_label(text="ITk pixel Internal", loc="upper left")
    ax1.text(0.2, 0.84, "KEKQ13 4.6A -100V", size=22, align=13)

    # Save the plot as a PDF
    fig.savefig("a1.png")

def histo_1d_signal_tdac_threshold(tdac, threshold):
    aplt.set_atlas_style()
    
    # Create a figure and axes
    fig, (ax1, ax2) = aplt.ratio_plot(name="fig1", figsize=(800, 800), hspace=0.05)
    
    sig_hist = root.TH1F("sig_hist", "TDAC", 55, 1000, 2100)
    sig2_hist = root.TH1F("sig2_hist", "Threshold", 55, 1000, 2100)
    data_hist = root.TH1F("data_hist", "TDAC_THRESHOLD", 55, 1000,2100)
    # Fill the map value
    # Here is the core for filling this map
    # other parts are just some style code
    for i, v in enumerate(tdac):
        a = 1000+i*20
        t0 = threshold[i]
        for m,n in enumerate(v):
            t1 = t0[m]
            for j, k in enumerate(n):
                if k == 15 or k==-15:
                    sig_hist.Fill(a)
                    if t1[j] != 1: 
                       data_hist.Fill(a)
                if t1[j] == 1:
                    sig2_hist.Fill(a)
                    data_hist.Fill(a)
                    
    
    sig_hist.SetFillColor(root.kAzure+1)
    sig_hist.SetLineWidth(0)
    # sig_hist.Sumw2()

    sig2_hist.SetFillColor(root.kRed+1)
    sig2_hist.SetLineWidth(0)
    # sig2_hist.Sumw2()

    # Fill the Error bar for the total TDAC+Threshold
    # data_hist.Sumw2()
    data_hist.SetBinErrorOption(root.TH1.EBinErrorOpt.kPoisson)

    # Add Stack to different signal and Background
    # Maybe necessary later when dealing with multiple plots
    bkg_and_sig = root.THStack("bkg_and_sig", "")
    #bkg_and_sig.Add(sig_hist)
    bkg_and_sig.Add(sig2_hist)

    ax1.plot( sig_hist, label= "TDAC +/-15", labelfmt="F")
    ax1.plot( sig2_hist, label="Thres", labelfmt="F")
    

    # Plot the MC stat error as a hatched band
    err_band = aplt.root_helpers.hist_to_graph(
        bkg_and_sig.GetStack().Last(),
        show_bin_width=True
    )
    ax1.plot(err_band, "2", fillcolor=1, fillstyle=3254, linewidth=0)

    # Plot the data as a graph
    data_graph = aplt.root_helpers.hist_to_graph(data_hist)
    ax1.plot(data_graph, "P")

    # Use same x-range in lower axes as upper axes
    # Draw line at y=1 in ratio panel
    ax2.set_xlim(ax1.get_xlim())
    line = root.TLine(ax1.get_xlim()[0], 0.5, ax1.get_xlim()[1], 0.5)
    ax2.plot(line)

    # Plot the relative error on the ratio axes
    err_band_ratio = aplt.root_helpers.hist_to_graph(
        bkg_and_sig.GetStack().Last(),
        show_bin_width=True,
        norm=True
    )
    ax2.plot(err_band_ratio, "2", fillcolor=1, fillstyle=3254, label = "Total")

    # Calculate and draw the ratio
    ratio_hist = sig2_hist.Clone("ratio_hist")
    ratio_hist.Divide(data_hist)
    ratio_graph = aplt.root_helpers.hist_to_graph(ratio_hist)
    ax2.plot(ratio_graph, "P", label = "Total")

    # Add extra space at top of plot to make room for labels
    ax1.add_margins(top=0.16)

    # Set axis titles
    ax2.set_xlabel("Threshold Tuning [e]")
    ax1.set_ylabel("Counts")
    ax2.set_ylabel("Threshold / Overall", loc="centre")

    ax2.set_ylim(0.0, 1.0)

    # Go back to top axes to add labels
    ax1.cd()

    # Add the ATLAS Label
    aplt.atlas_label(text="ITk pixel Internal", loc="upper left")
    ax1.text(0.2, 0.84, "KEKQ13 4.6A -100V", size=22, align=13)

    # Add legend
    legend = ax1.legend(
        loc=(0.68, 0.65, 0.95, 0.92),textsize=22
    )

    # legend.AddEntry(data_graph, "data_hist", "l")
    # legend.AddEntry(sig_hist, "sig_hist", "l")
    # legend.AddEntry(sig2_hist, "sig2_hist", "l")
    # legend.AddEntry(err_band, "MC Stat. Unc.", "l")

    # Save the plot as a PDF
    fig.savefig("t6.png")

def combine_folder_name(a,b,c,d,e):
    ss = []
    for i,v in enumerate(a):
        ss.append(v)
        ss.append(b[i])
        ss.append(c[i])
        ss.append(d[i])
        ss.append(e[i])
    return ss

def combine_threshold_file(s,b):
    c = []
    for x in s:
        c.append(x+b)
    return c

def map_union(tdac_maps, threshold_maps):
    a = []
    d = []
    b = []
    t = 0
    for i,v in enumerate(tdac_maps):
        chip = threshold_maps[i]
        for n,m  in enumerate(v):
            col = chip[n]
            for j,k in enumerate(m):
                if k == -15 or k == 15:
                    d.append(1)
                else:
                    if col[j]==1:
                        d.append(1)
                    else:
                        d.append(0)
            b.append(d)
            d = []
        a.append(b)
        b = []
    return a


def map_intersection(tdac_maps, threshold_maps):
    a = []
    d = []
    b = []
    for i,v in enumerate(tdac_maps):
        chip = threshold_maps[i]
        for n,m  in enumerate(v):
            col = chip[n]
            for j,k in enumerate(m):
                if k == -15 or k == 15:
                    if col[j]==1:
                        d.append(1)
                    else:
                        d.append(0)
                else:
                    d.append(0)
            b.append(d)
            d = []
        a.append(b)
        b = []
    return a


def map_union_(noise_maps, threshold_maps):
    a = []
    d = []
    b = []
    t = 0
    for i,v in enumerate(noise_maps):
        chip = threshold_maps[i]
        for n,m  in enumerate(v):
            col = chip[n]
            for j,k in enumerate(m):
                if k == 1:
                    d.append(1)
                else:
                    if col[j]==1:
                        d.append(1)
                    else:
                        d.append(0)
            b.append(d)
            d = []
        a.append(b)
        b = []
    return a

def map_intersection_(noise_maps, threshold_maps):
    a = []
    d = []
    b = []
    for i,v in enumerate(noise_maps):
        chip = threshold_maps[i]
        for n,m  in enumerate(v):
            col = chip[n]
            for j,k in enumerate(m):
                if k ==1 :
                    if col[j]==1:
                        d.append(1)
                    else:
                        d.append(0)
                else:
                    d.append(0)
            b.append(d)
            d = []
        a.append(b)
        b = []
    return a

def get_m1_from_m2(a_maps,b_maps):
    a = []
    d = []
    b = []
    for i,v in enumerate(a_maps):
        chip = b_maps[i]
        for n,m  in enumerate(v):
            col = chip[n]
            for j,k in enumerate(m):
                if k ==1 :
                    d.append(col[j])
                else:
                    d.append(1000)
            b.append(d)
            d = []
        a.append(b)
        b = []
    return a