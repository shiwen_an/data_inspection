# Author: Shiwen An
# Date: 2021 Nov. 25th
# Purpose: To find out the important statistical error after threshold
#          Tuning

import json
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from collections import deque
import math
from collections import deque


def threshold_repeat( tmaps):
    cnt_ = 0
    raw = tmaps[0]
    for x2 in tmaps:
        for i, v in enumerate(x2): 
                for j,x in enumerate(v):
                    if x == 1:
                        raw[i][j] = raw[i][j]+1        

    for i1, x1 in enumerate(raw):
        for j1, y1 in enumerate(x1):
            if  y1 >= (len(tmaps)):
                raw[i1][j1] =1
                cnt_ = cnt_+1
            else:
                raw[i1][j1] =0

    print(cnt_)
    plt.imshow(raw, interpolation='none')
    plt.show()
    plt.savefig('threshold_repeat.png')
    return raw

# return loose selection without image output
def threshold_occ_stat(chips, start_col):
    # Find out the problem on Occupancy map
    b2 = []
    d = []
    avg = []
    std = []
    all_chips = []
    cnt_ = 0
    # with open(file_s[2]+chip_config[1]) as json_file:
    for x in chips:
        with open(x) as json_file:
            occ = json.load(json_file)
        occ_map = occ.get('Data')
        a = get_mean(occ_map,start_col)
        s = get_std(occ_map, start_col,a)
        avg.append(a)
        std.append(s)
        three_sigma_plus = a+3*s
        three_sigma_minus = a-3*s
        for i, v in enumerate(occ_map): 
            if i >=start_col : 
                for i, v in enumerate(v):
                    if v > three_sigma_plus or v<three_sigma_minus: 
                        d.append(1)
                        cnt_ = cnt_ + 1
                    else:
                        d.append(0)
                b2.append(d)
                d = []

        b2 = np.transpose(b2)
        b2 = np.flip(b2, 0)
        all_chips.append(b2)
        print('Threshold Mean: ', a)
        print('Threshold Std: ', s)
        print('Threshold>3sigma: ', cnt_)
        print('Threshold>3sigma Percentage: ', cnt_/26112) # 192*136
        b2 = []
        cnt_ = 0
    return all_chips, avg, std

def threshold_occ_stat_u(chips, start_col):
    # Find out the problem on Occupancy map
    b2 = []
    d = []
    avg = []
    std = []
    all_chips = []
    cnt_ = 0
    # with open(file_s[2]+chip_config[1]) as json_file:
    for x in chips:
        with open(x) as json_file:
            occ = json.load(json_file)
        occ_map = occ.get('Data')
        a = get_mean(occ_map,start_col)
        s = get_std(occ_map, start_col,a)
        avg.append(a)
        std.append(s)
        three_sigma_plus = a+3*s
        three_sigma_minus = a-3*s
        for i, v in enumerate(occ_map): 
            if i >=start_col : 
                for i, v in enumerate(v):
                    if v > three_sigma_plus or v<three_sigma_minus: 
                        d.append(v)
                        cnt_ = cnt_ + 1
                    else:
                        d.append(1500)
                b2.append(d)
                d = []

        b2 = np.transpose(b2)
        b2 = np.flip(b2, 0)
        all_chips.append(b2)
        print('Threshold Mean: ', a)
        print('Threshold Std: ', s)
        print('Threshold>3sigma: ', cnt_)
        print('Threshold>3sigma Percentage: ', cnt_/26112) # 192*136
        b2 = []
        cnt_ = 0
    return all_chips, avg, std



def threshold_occ_stat_w(chips, start_col):
    # Find out the problem on Occupancy map
    b2 = []
    d = []
    avg = []
    std = []
    all_chips = []
    cnt_ = 0
    # with open(file_s[2]+chip_config[1]) as json_file:
    for x in chips:
        with open(x) as json_file:
            occ = json.load(json_file)
        occ_map = occ.get('Data')
        a = get_mean(occ_map,start_col)
        s = get_std(occ_map, start_col,a)
        avg.append(a)
        std.append(s)
        three_sigma_plus = a+3*s
        three_sigma_minus = a-3*s
        for i, v in enumerate(occ_map): 
            if i >=start_col : 
                for i, v in enumerate(v):
                    if v == 0:
                        d.append(0)
                    elif v > three_sigma_plus or v<three_sigma_minus: 
                        d.append(1)
                        cnt_ = cnt_ + 1
                    else:
                        d.append(0)
                b2.append(d)
                d = []

        b2 = np.transpose(b2)
        b2 = np.flip(b2, 0)
        all_chips.append(b2)
        print('Threshold Mean: ', a)
        print('Threshold Std: ', s)
        print('Threshold>3sigma: ', cnt_)
        print('Threshold>3sigma Percentage: ', cnt_/26112) # 192*136
        b2 = []
        cnt_ = 0
    return all_chips, avg, std

def threshold_occ_stat_zero(chips, start_col):
    # Find out the problem on Occupancy map
    b2 = []
    d = []
    avg = []
    std = []
    all_chips = []
    cnt_ = 0
    # with open(file_s[2]+chip_config[1]) as json_file:
    for x in chips:
        with open(x) as json_file:
            occ = json.load(json_file)
        occ_map = occ.get('Data')
        a = get_mean(occ_map,start_col)
        s = get_std(occ_map, start_col,a)
        avg.append(a)
        std.append(s)
        for i, v in enumerate(occ_map): 
            if i >=start_col : 
                for i, v in enumerate(v):
                    if v == 0:
                        d.append(1)
                    else:
                        d.append(0)
                b2.append(d)
                d = []
        b2 = np.transpose(b2)
        b2 = np.flip(b2, 0)
        all_chips.append(b2)
        print('Threshold Mean: ', a)
        print('Threshold Std: ', s)
        print('Threshold>3sigma: ', cnt_)
        print('Threshold>3sigma Percentage: ', cnt_/26112) # 192*136
        b2 = []
        cnt_ = 0
    return all_chips, avg, std

def plot_threshold_zero(chips):
    # Find out the problem on Occupancy map
    b2 = []
    d = []
    avg = []
    std = []
    all_chips = deque()
    cnt_ = 0
    # with open(file_s[2]+chip_config[1]) as json_file:
    for x in chips:
        with open(x) as json_file:
            occ = json.load(json_file)
        occ_map = occ.get('Data')
        for i, v in enumerate(occ_map): 
            if i >=264 : 
                for i, v in enumerate(v):
                    if v == 0: 
                        d.append(1)
                        cnt_ = cnt_ + 1
                    else:
                        d.append(0)
                b2.append(d)
                d = []
        a = get_mean(occ_map,264)
        s = get_std(occ_map, 264, a)
        print('Threshold Mean: ', a)
        print('Threshold Std: ', s)
        avg.append(a)
        std.append(s)
        b2 = np.transpose(b2)
        b2 = np.flip(b2, 0)
        all_chips.append(b2)
        b2 = []
        print('Threshold Zero: ', cnt_)
        print('Threshold Zero Percentage: ', cnt_/26112) # 192*136
        cnt_ = 0
    
    plt.suptitle('Threshold Map Zero Values \n')
    plt.subplot(221)
    plt.imshow(all_chips[0], interpolation='none')
    plt.title('Chip1 Diff')
    plt.subplot(223)
    plt.imshow(all_chips[1], interpolation='none')
    plt.title('Chip2 Diff')
    plt.subplot(224)
    plt.imshow(all_chips[2], interpolation='none')
    plt.title('Chip3 Diff')
    plt.subplot(222)
    plt.imshow(all_chips[3], interpolation='none')  
    plt.title('Chip4 Diff')  
    plt.show()

def get_mean(occ_map, col):
    cnt_ = 0
    avg  = 0
    for i, v in enumerate(occ_map): 
        if i >= col : 
            for i, v in enumerate(v):
                avg =  avg+v
                cnt_ = cnt_+1
    return (avg/cnt_)

def get_std(occ_map, col, avg):
    cnt_ = 0
    std = 0
    for i, v in enumerate(occ_map): 
        if i >= col : 
            for i, v in enumerate(v):
                std = std+(avg-v)*(avg-v)
                cnt_ = cnt_+1
    std =  math.sqrt(std/cnt_)
    return std

def plot_threshold_stat(chips):
    # Find out the problem on Occupancy map
    b2 = []
    d = []
    avg = []
    std = []
    all_chips = deque()
    cnt_ = 0
    ss = chips[0]
    # with open(file_s[2]+chip_config[1]) as json_file:
    for x in chips:
        with open(x) as json_file:
            occ = json.load(json_file)
        occ_map = occ.get('Data')
        a = get_mean(occ_map,264)
        s = get_std(occ_map, 264, a)
        print('Threshold Mean: ', a)
        print('Threshold Std: ', s)
        avg.append(a)
        std.append(s)
        three_sigma_plus = a+3*s
        three_sigma_minus = a-3*s
        for i, v in enumerate(occ_map): 
            if i >=264 : 
                for i, v in enumerate(v):
                    if v > three_sigma_plus or v<three_sigma_minus: 
                        d.append(1)
                        cnt_ = cnt_ + 1
                    else:
                        d.append(0)
                b2.append(d)
                d = []

        b2 = np.transpose(b2)
        b2 = np.flip(b2, 0)
        all_chips.append(b2)
        b2 = []
        print('Threshold Zero: ', cnt_)
        print('Threshold Zero Percentage: ', cnt_/26112) # 192*136
        cnt_ = 0
    
    plt.suptitle(ss[0:6]+' Threshold Map Over 3 Sigma Values \n')
    plt.subplot(221)
    plt.imshow(all_chips[0], interpolation='none')
    plt.title('Chip1 Diff')
    plt.subplot(223)
    plt.imshow(all_chips[1], interpolation='none')
    plt.title('Chip2 Diff')
    plt.subplot(224)
    plt.imshow(all_chips[2], interpolation='none')
    plt.title('Chip3 Diff')
    plt.subplot(222)
    plt.imshow(all_chips[3], interpolation='none')  
    plt.title('Chip4 Diff')  
    plt.show()
    return all_chips

# Plot out the tuning 3 sigma out of the range large only and zero
def plot_threshold_stat_large(chips):
    # Find out the problem on Occupancy map
    b2 = []
    d = []
    avg = []
    std = []
    all_chips = deque()
    cnt_ = 0
    ss = chips[0]
    # with open(file_s[2]+chip_config[1]) as json_file:
    for x in chips:
        with open(x) as json_file:
            occ = json.load(json_file)
        occ_map = occ.get('Data')
        a = get_mean(occ_map,264)
        s = get_std(occ_map, 264, a)
        print('Threshold Mean: ', a)
        print('Threshold Std: ', s)
        avg.append(a)
        std.append(s)
        three_sigma_plus = a+3*s
        three_sigma_minus = a-3*s
        for i, v in enumerate(occ_map): 
            if i >=264 : 
                for i, v in enumerate(v):
                    if v > three_sigma_plus or v==0: 
                        d.append(1)
                        cnt_ = cnt_ + 1
                    else:
                        d.append(0)
                b2.append(d)
                d = []

        b2 = np.transpose(b2)
        b2 = np.flip(b2, 0)
        all_chips.append(b2)
        b2 = []
        print('Threshold>3sigma: ', cnt_)
        print('Threshold>3sigma Percentage: ', cnt_/26112) # 192*136
        cnt_ = 0
    
    plt.suptitle(ss[0:6]+' Threshold Map Over 3 Sigma Values \n')
    plt.subplot(221)
    plt.imshow(all_chips[0], interpolation='none')
    plt.title('Chip1 Diff')
    plt.subplot(223)
    plt.imshow(all_chips[1], interpolation='none')
    plt.title('Chip2 Diff')
    plt.subplot(224)
    plt.imshow(all_chips[2], interpolation='none')
    plt.title('Chip3 Diff')
    plt.subplot(222)
    plt.imshow(all_chips[3], interpolation='none')  
    plt.title('Chip4 Diff')  
    plt.show()





def xray_threshold_intersection(xray_occ, threshold_occ):
    a = []
    d = []
    b = []
    cnt_ = 0
    cnt_xray = 0
    cnt_tot = 0
    for i,v in enumerate(xray_occ):
        chip = threshold_occ[i]
        print(i)
        for n,m  in enumerate(v):
            col = chip[n]
            for j,k in enumerate(m):
                if col[j] == 1:
                    cnt_tot = cnt_tot+1
                if k==1:
                    cnt_xray = cnt_xray+1
                if k == col[j] and k==1:
                    d.append(1)
                    cnt_ = cnt_ + 1
                else:
                    d.append(0)
            b.append(d)
            d = []
        a.append(b)

        print('Xray: ', cnt_xray) #
        print('Intersection: ', cnt_)
        print('Threshold: ', cnt_tot) # confirmation with input
        print('Percentage: ', cnt_/26112) # 192*136
        print('Inter/Tot: ', cnt_/cnt_tot) # No idea what the result could be
        b = []
        cnt_tot =0
        cnt_ = 0
        cnt_xray = 0
    
    
    plt.suptitle(' Threshold Map +/- 3 Sigma \n intersection Xray')
    plt.subplot(221)
    plt.imshow(a[0], interpolation='none')
    plt.title('Chip1 Diff')
    plt.subplot(223)
    plt.imshow(a[1], interpolation='none')
    plt.title('Chip2 Diff')
    plt.subplot(224)
    plt.imshow(a[2], interpolation='none')
    plt.title('Chip3 Diff')
    plt.subplot(222)
    plt.imshow(a[3], interpolation='none')  
    plt.title('Chip4 Diff')  
    plt.show()
    return a

# exclude occ1 from occ2
def xray_threshold_exclusion(occ1, occ2):
    a = []
    d = []
    b = []
    cnt_ = 0
    cnt_xray = 0
    cnt_tot = 0
    for i,v in enumerate(occ1):
        chip = occ2[i]
        for n,m  in enumerate(v):
            col = chip[n]
            for j,k in enumerate(m):
                if col[j] == 1:
                    cnt_tot = cnt_tot+1
                    if k==1:
                        d.append(0)
                    else:
                        d.append(1)
                        cnt_ = cnt_ + 1
                else:
                    d.append(0)
                if k==1:
                    cnt_xray = cnt_xray+1
                    
            b.append(d)
            d = []
        a.append(b)
        print()
        print('Xray: ', cnt_xray) #
        print('AfterExclusion: ', cnt_)
        print('Threshold: ', cnt_tot) # confirmation with input
        print('Percentage: ', cnt_/26112) # 192*136
        print('Inter/Tot: ', cnt_/cnt_tot) # No idea what the result could be
        b = []
        cnt_tot =0
        cnt_ = 0
        cnt_xray = 0
    
    
    plt.suptitle(' Threshold Map +/- 3 Sigma \n exclusion Xray')
    plt.subplot(221)
    plt.imshow(a[0], interpolation='none')
    plt.title('Chip1 Diff')
    plt.subplot(223)
    plt.imshow(a[1], interpolation='none')
    plt.title('Chip2 Diff')
    plt.subplot(224)
    plt.imshow(a[2], interpolation='none')
    plt.title('Chip3 Diff')
    plt.subplot(222)
    plt.imshow(a[3], interpolation='none')  
    plt.title('Chip4 Diff')  
    plt.show()
    return a


def tot_tdac_histo1d(tdac_occ, tot_occ):
    hist_x =[]
    cnt_ = 0
    for i,v in enumerate(tot_occ):
        tdac = tdac_occ[i]
        for n,m  in enumerate(v):
            tdac_col = tdac[n]
            for j,k in enumerate(m):
                if k == 1:
                    print(tdac_col[j])
                    hist_x.append(tdac_col[j])
                    cnt_ = cnt_+1
        print()
        print('AfterExclusion: ', cnt_)
        cnt_ = 0

    plt.hist(hist_x, density=True, bins=30)
    plt.title('Distribution of TDAC value for untuned pixels')
    plt.ylabel('Probability')
    plt.xlabel('TDAC Value')
    plt.show()