# Author: Shiwen An
# Date: 2021 Nov. 22nd
# Purpose: To Scan the TDAC value and visualize the min/max value
# and Read .json file 
# Some quite useful resource; sliding windows
# https://matplotlib.org/stable/gallery/widgets/range_slider.html#sphx-glr-gallery-widgets-range-slider-py
# link above

import json
# import atlas_mpl_style as ampl
import matplotlib
import matplotlib.pyplot as plt
from numpy.core.fromnumeric import std
import pandas as pd
import numpy as np
# import seaborn as sns
from collections import deque


from Xray import plot_xray_occupancy
from Xray import correlations

from Threshold import plot_threshold_stat_large
from Threshold import plot_threshold_zero
from Threshold import plot_threshold_stat
from Threshold import xray_threshold_intersection
from Threshold import xray_threshold_exclusion
from Threshold import tot_tdac_histo1d
from Threshold import get_std
from Threshold import get_mean

from atlas_example import histo_2d_signal_tdac
from atlas_example import histo_1d_signal_tdac
from atlas_example import histo_1d_signal_tdac_threshold

# setup the font for the picture output
font = {'family' : 'monospace',
        'weight' : 'bold',
        'size' : 25
        }

# matplotlib.rc('font', **font)
# plt.style.use('classic')
# plt.style.use('print')

def tdac_occ(chip_configs, start_col):
    b2 = []
    all_chips = []
    avg = []
    std = []
    for x2 in chip_configs:
        with open(x2) as json_file:
            data_before = json.load(json_file)
        tdac_map = data_before.get('RD53A')
        tdac_map = tdac_map.get('PixelConfig')

        for i, v in enumerate(tdac_map): 
            if i >=start_col : 
                tdac_col = tdac_map[i]
                tdac_col = tdac_col['TDAC']
                b2.append(tdac_col)
    
        a = get_mean(b2, 0)
        s = get_std(b2, 0, a)
        avg.append(a)
        std.append(s)
        b2 = np.transpose(b2)
        b2 = np.flip(b2, 0)
        all_chips.append(b2)        
        b2 = []
    return all_chips, avg, std

def tdac_repeat(chip_configs, start_col):
    b2 = []
    all_chips = []
    map = [[0 for x in range(192)] for y in range(400-start_col)]
    print(len(map))
    for x2 in chip_configs:
        with open(x2) as json_file:
            data_before = json.load(json_file)
        tdac_map = data_before.get('RD53A')
        tdac_map = tdac_map.get('PixelConfig')

        for i, v in enumerate(tdac_map): 
            if i >=start_col : 
                tdac_col = v
                tdac_col = tdac_col['TDAC']
                for j,x in enumerate(tdac_col):
                    if x == -15 or x==15:
                        map[i-start_col][j] = map[i-start_col][j]+1
                b2.append(tdac_col)
        b2 = np.transpose(b2)
        b2 = np.flip(b2, 0)
        all_chips.append(b2)        
        b2 = []
    cnt_ =0
    for i1, x1 in enumerate(map):
        for j1, y1 in enumerate(x1):
            if map[i1][j1] !=55 :
                map[i1][j1] =0
            else:
                cnt_ = cnt_+1

    print(cnt_)
    plt.imshow(map, interpolation='none')
    plt.show()
    plt.savefig('repeat.png')
    return map


def tdac_occ_stat(chip_configs, start_col):
    b2 = []
    all_chips = []
    avg = []
    std = []
    d = []
    for x2 in chip_configs:
        with open(x2) as json_file:
            data_before = json.load(json_file)
        tdac_map = data_before.get('RD53A')
        tdac_map = tdac_map.get('PixelConfig')

        for i, v in enumerate(tdac_map): 
            if i >=start_col : 
                tdac_col = tdac_map[i]
                tdac_col = tdac_col['TDAC']
                for x in tdac_col:
                    if x ==15 or x == -15:
                        d.append(1)
                    else:
                        d.append(0)
                b2.append(d)
                d = []
                
        a = get_mean(b2, 0)
        s = get_std(b2, 0, a)
        avg.append(a)
        std.append(s)
        b2 = np.transpose(b2)
        b2 = np.flip(b2, 0)
        all_chips.append(b2)        
        b2 = []

    return all_chips, avg, std

def plot_tdac(filename):
    with open(filename) as json_file:
        data_before = json.load(json_file)
    b = []
    b1 = []
    c = []
    d = []
    tdac_map = data_before.get('RD53A')
    tdac_map = tdac_map.get('PixelConfig')

    for i, v in enumerate(tdac_map):
        print(i)
        if i >= 128 :
            c = v['TDAC']
            for i, l in enumerate(c):
                if l == -15 or l == 15:
                    d.append(1)
                else:
                    d.append(0)
            b.append(v['TDAC'])
            b1.append(d)
            d = []
    print(b)
    print(b1)
    plt.imshow(b1, interpolation='none')
    plt.show()
    plt.imshow(b, interpolation='none')
    plt.show()

# 1 dimensional histogram checker to show different tdac result
def plot_tdac_histo1d(tdac_occ):
    hist_x =[]
    cnt_ = 0
    for i,v in enumerate(tdac_occ):
        for n,m  in enumerate(v):
            for j,k in enumerate(m):
                    hist_x.append(k)
                    cnt_ = cnt_+1
        print()
        print('AfterExclusion: ', cnt_)
        cnt_ = 0

    plt.hist(hist_x, density=True, bins=30)
    plt.title('Distribution of TDAC value all chips  \n'+
    '1500e threshold Tuning')
    plt.ylabel('Probability')
    plt.xlabel('TDAC Value')
    plt.show()


    

def combine_string(s, b):
    c = []
    for x in b:
        c.append(s+x)
    return c

def threshold_investigate(s):
    plot_threshold_zero(s)
    print()
    plot_threshold_stat_large(s)
    print()
    a = plot_threshold_stat(s)

def tdac_investigate(s):
    tdac, avg, std= tdac_occ(s)
    plot_tdac_histo1d(tdac)

def histo_1d_2d_tdac_threshold(chip_configs):
    all_chips, avg, std = tdac_occ(chip_configs)
    histo_2d_signal_tdac(all_chips)
    histo_1d_signal_tdac(all_chips)

def tdac_threshold_investigate(s1, s2):
    tdac, avg, std = tdac_occ(s1)
    threshold, avg, std = plot_threshold_stat_list(s2)
    if len(tdac) == len(threshold):
        print("Oh Yeah! Baby.")
    histo_1d_signal_tdac_threshold(tdac, threshold)


def main():
    # KEKQ18 tuning
    file_s = ['KEKQ18/211004_026/049383_std_thresholdscan/',  #1000e
          'KEKQ18/211004_027/049399_std_thresholdscan/',  #1500e
          'KEKQ18/211004_028/049419_std_thresholdscan/',  #2000e
          'KEKQ18/211004_029/049422_std_xrayscan/',
          'KEKQ07/211121_007/052883_diff_thresholdscan/', #1500e 200TCs
          'KEKQ07/211121_007/052884_lin_thresholdscan/',
          'KEKQ07/211121_015/052906_lindiff_noisescan/'
    ]

    file_KEKQ07 = [
        'KEKQ07/211112_048/052511_diff_thresholdscan/', #1500e 100TCs
        'KEKQ07/211112_048/052512_lin_thresholdscan/',
        'KEKQ07/211112_049/052519_lindiff_noisescan/',
        'KEKQ07/211115_001/052525_diff_thresholdscan/', #1500e 125TCs
        'KEKQ07/211115_001/052526_lin_thresholdscan/',
        'KEKQ07/211115_002/052536_lindiff_noisescan/',        
        'KEKQ07/211117_002/052610_diff_thresholdscan/', #1500e 150TCs
        'KEKQ07/211117_002/052611_lin_thresholdscan/',
        'KEKQ07/211117_003/052617_lindiff_noisescan/',
        'KEKQ07/211119_006/052701_diff_thresholdscan/', #1500e 175TCs
        'KEKQ07/211119_006/052702_lin_thresholdscan/',
        'KEKQ07/211119_007/052707_lindiff_noisescan/',
        'KEKQ07/211121_007/052883_diff_thresholdscan/', #1500e 200TCs
        'KEKQ07/211121_007/052884_lin_thresholdscan/',
        'KEKQ07/211121_015/052906_lindiff_noisescan/'
    ]

    chip_config = ['chip1.json.before','chip1_ThresholdMap-0.json','chip1_Occupancy.json']
    all_chips = ['chip1_Occupancy.json','chip2_Occupancy.json','chip3_Occupancy.json','chip4_Occupancy.json']
    chip_configs_before = ['chip1.json.before','chip2.json.before','chip3.json.before','chip4.json.before']
    chip_threshold = ['chip1_ThresholdMap-0.json','chip2_ThresholdMap-0.json','chip3_ThresholdMap-0.json','chip4_ThresholdMap-0.json']
    # Make necessary string list:

    
    chip_configs = combine_string(file_s[4],chip_configs_before) # Let me take statistics for 
                                                                 # different 
    chips_tot = combine_string(file_s[1], chip_threshold)

    chips_tot_KEKQ07 = combine_string(file_s[4], chip_threshold)
    chips_xray_KEKQ07 = combine_string(file_s[6], all_chips)
    

    # Call necessary functions:

    xray_occ = plot_xray_occupancy(chips_xray_KEKQ07)
    threshold_occ = plot_threshold_stat(chips_tot_KEKQ07)
    xray_tot_occ = xray_threshold_intersection(xray_occ, threshold_occ)
    xray_tot_occ_ecl = xray_threshold_exclusion(xray_tot_occ, threshold_occ)
    tdac = tdac_occ(chip_configs)
    plot_tdac_histo1d(tdac)
    tot_tdac_histo1d(tdac, xray_tot_occ_ecl)

    # threshold_investigate(chips_tot_KEKQ07)
    # threshold_investigate(chips_tot)

    # plot_occupancy(chips[0])
    
    # correlations(chips,chip_configs)
    # plot_tdac(file_s[0]+chip_config[0])
    # plot_occupancy(file_s[0]+chip_config[1])

if __name__ == "__main__" :
    main()