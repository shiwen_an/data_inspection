# Author: Shiwen An 
# Date: 2021/11/28
# Purpose: Storing the example code for the physics analysis
#          Somehow might be useful in the future
import ROOT as root
import atlasplots as aplt
import matplotlib.pyplot as plt
import numpy as np

# ATLAS Python Format
# Nice library helps people in the group out
# https://atlas-plots.readthedocs.io/en/latest/examples.html


# Three different value imitate the atlas plot
def avg_std_graph(start, bin, avg,std, pdf_name, xlabel, ylabel):
    x = []
    y = []
    aplt.set_atlas_style()
    fig, ax = aplt.subplots(1, 1, name="fig1", figsize=(800, 600))
    for i,v in enumerate(avg):
        x.append(i*bin+start)
        y.append(v)
    yerr = std
    graph = ax.graph(x,y, yerr = yerr, label = "Avg", figsize=(800,600))
    graph.Fit("pol1", "0") # should be some linear fit
    ax.add_margins(top=0.18, left=0.05, right=0.05, bottom=0.05)

    func = graph.GetFunction("pol1")
    func.SetRange(*ax.get_xlim())
    func.SetNpx(1000)
    ax.plot(func, linecolor=root.kRed+1, expand=False, label="Fit", labelfmt="L")

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    aplt.atlas_label(text="ITk pixel Internal", loc="upper left")
    ax.text(0.2, 0.84, "KEKQ13 Chip4 4.6A, -100V", size=22, align=13)

    ax.legend(loc=(0.65, 0.8, 0.95, 0.92))
    fig.savefig(pdf_name)


# Three different value imitate the atlas plot
def avg_Threshold_graph(start, bin, avg,std, graph_name):
    x = []
    y = []
    aplt.set_atlas_style()
    fig, ax = aplt.subplots(1, 1, name="fig1", figsize=(800, 600))
    for i,v in enumerate(avg):
        x.append(i*bin+start)
        y.append(v)
    yerr = std
    graph = ax.graph(x,y, yerr = yerr, label = "Avg", figsize=(800,600))
    graph.Fit("pol1", "0") # should be some linear fit
    ax.add_margins(top=0.18, left=0.05, right=0.05, bottom=0.05)

    func = graph.GetFunction("pol1")
    func.SetRange(*ax.get_xlim())
    func.SetNpx(1000)
    ax.plot(func, linecolor=root.kRed+1, expand=False, label="Fit", labelfmt="L")

    ax.set_xlabel("Tuning Setup [e]")
    ax.set_ylabel("After Tuning [e]")

    aplt.atlas_label(text="ITk pixel Internal", loc="upper left")
    ax.text(0.2, 0.84, "KEKQ13 Chip4 4.6A, -100V", size=22, align=13)

    ax.legend(loc=(0.65, 0.8, 0.95, 0.92))
    fig.savefig(graph_name)

# Suppose the Prediction of TDAC setup finally should be a
# Gaussian Function
# How is the final TDAC value is going to match the result
def histo_signal(map):
    aplt.set_atlas_style()
    
    # Create a figure and axes
    fig, (ax1, ax2) = aplt.ratio_plot(name="fig1", figsize=(800, 800), hspace=0.05)

    # Define "signal" and data distribution functions
    sig_func = root.TF1("sig_func", "x*gaus(0)",-16,16)
    sig_func.SetParameters(0.1,-1,4) # set parameters for root gaussian

    sig_hist = root.TH1F("sig_hist", "Signal",32,-16,16)
    
    # Fill the map value
    # Here is the core for filling this map
    # other parts are just some style code
    for i,v in enumerate(map):
        for x in v:
            sig_hist.Fill(x)
    
    sig_hist.SetFillColor(root.kAzure+1)
    sig_hist.SetLineWidth(0)
    sig_hist.Sumw2()

    # Fill the Prediction data value
    data_hist = root.TH1F("data_hist", "Data", 32,-16,16)
    data_hist.FillRandom("sig_func",26112)# Hard coding here, TODO change to pixel row*col
    data_hist.Sumw2()
    data_hist.SetBinErrorOption(root.TH1.EBinErrorOpt.kPoisson)  # Use 68% Poisson errors

    # Add Stack to different signal and Background
    # Maybe necessary later when dealing with multiple plots
    bkg_and_sig = root.THStack("bkg_and_sig", "")
    bkg_and_sig.Add(sig_hist)

    ax1.plot(bkg_and_sig)

    # Plot the MC stat error as a hatched band
    # not sure whether add it or not
    err_band = aplt.root_helpers.hist_to_graph(
        bkg_and_sig.GetStack().Last(),
        show_bin_width=True
    )
    ax1.plot(err_band, "2", fillcolor=1, fillstyle=3254, linewidth=0)

    # Plot the data as a graph
    data_graph = aplt.root_helpers.hist_to_graph(data_hist)
    ax1.plot(data_graph, "P")

    # Use same x-range in lower axes as upper axes
    # Draw line at y=1 in ratio panel
    ax2.set_xlim(ax1.get_xlim())
    line = root.TLine(ax1.get_xlim()[0], 1, ax1.get_xlim()[1], 1)
    ax2.plot(line)

    # Plot the relative error on the ratio axes
    err_band_ratio = aplt.root_helpers.hist_to_graph(
        bkg_and_sig.GetStack().Last(),
        show_bin_width=True,
        norm=True
    )
    ax2.plot(err_band_ratio, "2", fillcolor=1, fillstyle=3254)

    # Calculate and draw the ratio
    ratio_hist = data_hist.Clone("ratio_hist")
    ratio_hist.Divide(bkg_and_sig.GetStack().Last())
    ratio_graph = aplt.root_helpers.hist_to_graph(ratio_hist)
    ax2.plot(ratio_graph, "P")

    # Add extra space at top of plot to make room for labels
    ax1.add_margins(top=0.16)

    # Set axis titles
    ax2.set_xlabel("TDAC [-15,15]")
    ax1.set_ylabel("Events")
    ax2.set_ylabel("Events / Pred.", loc="centre")

    ax2.set_ylim(0.55, 1.45)

    # Go back to top axes to add labels
    ax1.cd()

    # Add the ATLAS Label
    aplt.atlas_label(text="ITk pixel Internal", loc="upper left")
    ax1.text(0.2, 0.84, "KEKQ13 4.6A -100V", size=22, align=13)

    # Save the plot as a PDF
    fig.savefig("tdac_test.pdf")

def histo_2d_signal_tdac(maps):
    aplt.set_atlas_style()
    fig, ax = aplt.subplots(1, 1, name="fig1", figsize=(800, 600))

    hist = root.TH2F("hist", "TDAC 2D Map",55, 1000,2100, 32, -16, 16)
    for i, v in enumerate(maps):
        for m,n in enumerate(v):
            for k in n:
                a = 1000+i*20
                hist.Fill(a,k)

    # Draw the histogram on these axes
    ax.plot2d(hist, "COLZ")

    # Change pad margins to allow space for z-axis colour bar and for ATLAS label
    ax.set_pad_margins(right=0.20, top=0.08)

    # Set axis titles
    ax.set_xlabel("Threshold Tuning [e]")
    ax.set_ylabel("TDAC Value")
    ax.set_zlabel("Events", titleoffset=1.2)

    # Add the ATLAS Label
    aplt.atlas_label(ax.pad.GetLeftMargin(), 0.97, text="ITk pixel Internal", align=13)
    ax.text(1 - ax.pad.GetRightMargin(), 0.97, "KEKQ13 4.6A 100V", size=22, align=33)

    # Save the plot as a PDF
    fig.savefig("b1.png")
    
def histo_1d_signal_tdac(maps):
    aplt.set_atlas_style()
    
    # Create a figure and axes
    fig, (ax1, ax2) = aplt.ratio_plot(name="fig1", figsize=(800, 800), hspace=0.05)
    sig_hist = root.TH1F("sig_hist", "TDAC", 55, 1000, 2100)
    data_hist = root.TH1F("data_hist", "TDAC_THRESHOLD", 55, 1000,2100)
    
    # Fill the map value
    # Here is the core for filling this map
    # other parts are just some style code
    for i, v in enumerate(maps):
        a = 1000+i*20
        for m,n in enumerate(v):
            for k in n:
                data_hist.Fill(a)
                if k == 15 or k==-15:
                    sig_hist.Fill(a)
    
    sig_hist.SetFillColor(root.kAzure+1)
    sig_hist.SetLineWidth(0)
    sig_hist.Sumw2()

    # Fill the Error bar for the total TDAC+Threshold
    data_hist.Sumw2()
    data_hist.SetBinErrorOption(root.TH1.EBinErrorOpt.kPoisson)

    # Add Stack to different signal and Background
    # Maybe necessary later when dealing with multiple plots
    bkg_and_sig = root.THStack("bkg_and_sig", "")
    bkg_and_sig.Add(sig_hist)
    ax1.plot(bkg_and_sig)

    # Plot the MC stat error as a hatched band
    # not sure whether add it or not
    err_band = aplt.root_helpers.hist_to_graph(
        bkg_and_sig.GetStack().Last(),
        show_bin_width=True
    )
    ax1.plot(err_band, "2", fillcolor=1, fillstyle=3254, linewidth=0)

    # Use same x-range in lower axes as upper axes
    # Draw line at y=0.05 in ratio panel
    ax2.set_xlim(ax1.get_xlim())
    line = root.TLine(ax1.get_xlim()[0], 0.05, ax1.get_xlim()[1], 0.05)
    ax2.plot(line)

    # Plot the relative error on the ratio axes
    err_band_ratio = aplt.root_helpers.hist_to_graph(
        bkg_and_sig.GetStack().Last(),
        show_bin_width=True,
        norm=True
    )
    ax2.plot(err_band_ratio, "2", fillcolor=1, fillstyle=3254)

    # Calculate and draw the ratio
    ratio_hist = sig_hist.Clone("ratio_hist")
    ratio_hist.Divide(data_hist)
    ratio_graph = aplt.root_helpers.hist_to_graph(ratio_hist)
    ax2.plot(ratio_graph, "P")

    # Add extra space at top of plot to make room for labels
    ax1.add_margins(top=0.16)
    # Legend = root.Tlegend(0.65, 0.78, 0.92, 0.90)
    # Legend.AddEntry(sig_hist, "TDAC", "f")
    # Legend.Draw()

    # Set axis titles
    ax2.set_xlabel("Threshold Tuning [e]")
    ax1.set_ylabel("TDAC +/- 15")
    ax2.set_ylabel("Events / Pixels #", loc="centre")

    ax2.set_ylim(0, 0.1)

    # Go back to top axes to add labels
    ax1.cd()

    # Add the ATLAS Label
    aplt.atlas_label(text="ITk pixel Internal", loc="upper left")
    ax1.text(0.2, 0.84, "KEKQ13 4.6A -100V", size=22, align=13)

    # Save the plot as a PDF
    fig.savefig("a1.png")

def histo_1d_signal_tdac_threshold(tdac, threshold):
    aplt.set_atlas_style()
    
    # Create a figure and axes
    fig, (ax1, ax2) = aplt.ratio_plot(name="fig1", figsize=(800, 800), hspace=0.05)
    
    sig_hist = root.TH1F("sig_hist", "TDAC", 55, 1000, 2100)
    sig2_hist = root.TH1F("sig2_hist", "Threshold", 55, 1000, 2100)
    data_hist = root.TH1F("data_hist", "TDAC_THRESHOLD", 55, 1000,2100)
    # Fill the map value
    # Here is the core for filling this map
    # other parts are just some style code
    for i, v in enumerate(tdac):
        a = 1000+i*20
        t0 = threshold[i]
        for m,n in enumerate(v):
            t1 = t0[m]
            for j, k in enumerate(n):
                if k == 15 or k==-15:
                    sig_hist.Fill(a)
                    data_hist.Fill(a)
                    if t1[j] == 1:
                        data_hist.Fill(a,-1)

                if t1[j] == 1:
                    sig2_hist.Fill(a)
                    data_hist.Fill(a)
                    
    
    sig_hist.SetFillColor(root.kAzure+1)
    sig_hist.SetLineWidth(0)
    # sig_hist.Sumw2()

    sig2_hist.SetFillColor(root.kRed+1)
    sig2_hist.SetLineWidth(0)
    # sig2_hist.Sumw2()

    # Fill the Error bar for the total TDAC+Threshold
    # data_hist.Sumw2()
    data_hist.SetBinErrorOption(root.TH1.EBinErrorOpt.kPoisson)

    # Add Stack to different signal and Background
    # Maybe necessary later when dealing with multiple plots
    bkg_and_sig = root.THStack("bkg_and_sig", "")
    bkg_and_sig.Add(sig_hist)
    bkg_and_sig.Add(sig2_hist)

    ax1.plot(bkg_and_sig)

    # Plot the MC stat error as a hatched band
    err_band = aplt.root_helpers.hist_to_graph(
        bkg_and_sig.GetStack().Last(),
        show_bin_width=True
    )
    ax1.plot(err_band, "2", fillcolor=1, fillstyle=3254, linewidth=0)

    # Plot the data as a graph
    data_graph = aplt.root_helpers.hist_to_graph(data_hist)
    ax1.plot(data_graph, "P")

    # Use same x-range in lower axes as upper axes
    # Draw line at y=1 in ratio panel
    ax2.set_xlim(ax1.get_xlim())
    line = root.TLine(ax1.get_xlim()[0], 1, ax1.get_xlim()[1], 1)
    ax2.plot(line)

    # Plot the relative error on the ratio axes
    err_band_ratio = aplt.root_helpers.hist_to_graph(
        bkg_and_sig.GetStack().Last(),
        show_bin_width=True,
        norm=True
    )
    ax2.plot(err_band_ratio, "2", fillcolor=1, fillstyle=3254)

    # Calculate and draw the ratio
    ratio_hist = data_hist.Clone("ratio_hist")
    ratio_hist.Divide(bkg_and_sig.GetStack().Last())
    ratio_graph = aplt.root_helpers.hist_to_graph(ratio_hist)
    ax2.plot(ratio_graph, "P")

    # Add extra space at top of plot to make room for labels
    ax1.add_margins(top=0.16)

    # Set axis titles
    ax2.set_xlabel("Threshold Tuning [e]")
    ax1.set_ylabel("Counts")
    ax2.set_ylabel("Threshold / Overall", loc="centre")

    ax2.set_ylim(0.55, 1.45)

    # Go back to top axes to add labels
    ax1.cd()

    # Add the ATLAS Label
    aplt.atlas_label(text="ITk pixel Internal", loc="upper left")
    ax1.text(0.2, 0.84, "KEKQ13 4.6A -100V", size=22, align=13)

    # Add legend
    legend = ax1.legend(
        loc=(0.68, 0.65, 0.95, 0.92),textsize=22
    )

    # legend.AddEntry(data_graph, "data_hist", "l")
    # legend.AddEntry(sig_hist, "sig_hist", "l")
    # legend.AddEntry(sig2_hist, "sig2_hist", "l")
    # legend.AddEntry(err_band, "MC Stat. Unc.", "l")

    # Save the plot as a PDF
    fig.savefig("a2.png")