# Author: Shiwen An
# Date: 2021 Nov. 22nd
# Purpose: To Scan the TDAC value and visualize the min/max value
# and Read .json file 
# Some quite useful resource; sliding windows
# https://matplotlib.org/stable/gallery/widgets/range_slider.html#sphx-glr-gallery-widgets-range-slider-py
# link above

import json
# import atlas_mpl_style as ampl
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from matplotlib.widgets import RangeSlider

import ROOT as root
import atlasplots as aplt

# import seaborn as sns
from collections import deque

from libUtil import avg_std_graph, histo_1d_multiple, histo_1d_ratio_threshold_avg_line, histo_1d_signal_tdac_threshold, histo_1d_simple, map_intersection_, map_union_
from libUtil import histo_2d_signal
from libUtil import histo_1d_ratio_tdac
from libUtil import histo_1d_ratio_threshold
from libUtil import histo_1d_simple
from libUtil import histo_1d_multiple
from libUtil import combine_folder_name
from libUtil import combine_threshold_file
from libUtil import map_intersection
from libUtil import map_union
from libUtil import histo_2d_number
from libUtil import readfile
from libUtil import graph_1d
from libUtil import graph_1d_simple
from libUtil import histo_2d_s_curve
from libUtil import histo_1d_simple_th
from libUtil import get_m1_from_m2
from libUtil import histo_1d_simple_0
# from libUtil import histo_1d_simple_th0

from Xray import plot_xray_occupancy

# from Xray import correlations


from Threshold import threshold_occ_stat, threshold_repeat
from Threshold import plot_threshold_stat_large
from Threshold import plot_threshold_zero
from Threshold import plot_threshold_stat
from Threshold import xray_threshold_intersection
from Threshold import xray_threshold_exclusion
from Threshold import tot_tdac_histo1d
from Threshold import threshold_occ_stat_u
from Threshold import threshold_occ_stat_w
from Threshold import threshold_occ_stat_zero


from Noise import noise_occ_stat
from Noise import noise_repeat
from Noise import noise_occ
from Noise import get_noise_th

from TDAC_Corr import tdac_occ
from TDAC_Corr import tdac_repeat
from TDAC_Corr import tdac_occ_stat
from TDAC_Corr import combine_string
from TDAC_Corr import plot_tdac_histo1d
from TDAC_Corr import tdac_investigate
from TDAC_Corr import tdac_threshold_investigate
from TDAC_Corr import histo_1d_2d_tdac_threshold

from S_Curve import get_point_xy
from S_Curve import get_point_n_xy
from S_Curve import get_s_curve
from S_Curve import get_s_curve_all
from S_Curve import get_points_s_curve
from S_Curve import get_points_s_curve_analog


# setup the font for the picture output
font = {'family' : 'monospace',
        'weight' : 'bold',
        'size' : 25
        }

# matplotlib.rc('font', **font)
# plt.style.use('classic')
# plt.style.use('print')

# KEKQ13 threshold correlations
def noise_threshold_corr(chip_threshold, chip_noise):
    nmap, avg, std = noise_occ_stat(chip_noise, 264)
    tmap, avg, std = threshold_occ_stat(chip_threshold, 264)
    map1 = map_union_(nmap,tmap)
    map2 = map_intersection_(nmap,tmap)
    histo_1d_ratio_threshold(map1, 65, 800,2100, "t4_.png", "Threshold[e]", "Threshold or Noise > 3#sigma", "Noisy")
    histo_1d_ratio_threshold(map2, 65, 800,2100, "t5_.png", "Threshold[e]", "Threshold and Noise > 3#sigma", "Bad") 


# One line command help make the s_curve files to read 
# cat | find /home/shiwen/RD53A/Data/KEKQ13/211210_140/last_scan -name "*Occupancy*.json" 
# >> /home/shiwen/RD53A/Data/KEKQ13/211210_140/last_scan/s_curve.txt
# Somehow helpful
def test_12_10_():
    # first try 1000e
    # threshold = ["/home/shiwen/RD53A/Data/KEKQ13/211210_142/last_scan"]
    # analog = ["/home/shiwen/RD53A/Data/KEKQ13/211210_142/055763_diff_analogscan"]
    # s_curve_files = readfile("/home/shiwen/RD53A/Data/KEKQ13/211210_142/last_scan/s_curve.txt")

    # 1500e 200 InjVCal 
    threshold = ["/home/shiwen/RD53A/Data/KEKQ13/211210_144/last_scan"]
    s_curve_files = readfile("/home/shiwen/RD53A/Data/KEKQ13/211210_144/last_scan/s_curve.txt")
    analog = ["/home/shiwen/RD53A/Data/KEKQ13/211210_144/055771_diff_analogscan"]

    # threshold = ["/home/shiwen/RD53A/Data/KEKQ13/211210_140/last_scan"]
    # s_curve_files = readfile("/home/shiwen/RD53A/Data/KEKQ13/211210_140/last_scan/s_curve.txt")

    chip_c = combine_threshold_file(threshold, chip_threshold_[2])
    chip_a = combine_threshold_file(analog,  chip_analog_[2])
    chip_n = combine_threshold_file(threshold, chip_noise_[2])

    # nmap = noise_occ(chip_n, 264)
    # tmap0, avg, std = threshold_occ_stat_zero(chip_c,264)
    # nmap_tmap0 = get_m1_from_m2(tmap0, nmap)
    # #histo_1d_simple(nmap_tmap0[0], 50, 0, 500, "t_n.png", "Noise[e]", "Count", "1500e")
    # histo_1d_simple_0(nmap_tmap0[0], 50, 0, 1000, "t_n.png", "Noise[e]", "Count", "1500e")

    tmap, point_3d, point0, point_n = get_points_s_curve(chip_c,264)
    amap, point_3d_a, point0_a, point_n_a = get_points_s_curve_analog(chip_a, 264)

    tmap_u, avg, std = threshold_occ_stat_u(chip_c,264)
    histo_1d_simple_th(tmap_u[0], 100, 0, 2000, "t0.png", "Threshold[e]", "Count", "1000e")
    # histo_1d_simple_th0(tmap_u, 100, 0, 200, "t0.png", "Threshold[e]", "Count", "1500e Tuning ")    

    # Points in Threshold Scan NOT in Analog Scan
    point_3d_e = list(set(point_3d_a)-set(point_3d))
    point0_e = list(set(point0_a)-set(point0))  
    point_n_e  = list(set(point0_a)-set(point0))  

    ll = []
    ll.append(len(point_3d))
    ll.append(len(point0))
    ll.append(len(point_n))
    ll.append(len(point_3d_a))
    ll.append(len(point0_a))
    ll.append(len(point_n_a))

    ll.append(len(point_3d_e))
    ll.append(len(point0_e))
    ll.append(len(point_n_e))
 
    print("[Debug Threshold] 3std, 0, normal",ll[0],ll[1],ll[2])
    print("[Debug Analog] abnormal, 0, normal",ll[3],ll[4],ll[5])
    print("[Debug] In Th. NOT in Analog 3std, 0, normal", ll[6],ll[7],ll[8])

    d1 = get_s_curve_all(point_3d,s_curve_files, 0)
    d2 = get_s_curve_all(point0,s_curve_files, 0)
    d3 = get_s_curve_all(point_n, s_curve_files, 0)

    p1,p2,p3 = [],[],[]
    for i in range(10):
        p1.append(d1[np.random.randint( ll[0] )])
        p2.append(d2[np.random.randint( ll[1] )])
        p3.append(d3[np.random.randint( ll[2] )])

    a = np.histogram(d1[0])
    x = np.array(range(len(d1[0])))
    print(d1[0])

    graph_1d_simple(x,p1,"InjVCal", "Counts",  "a.png" , "S-Curve Untuned |Thr.-Avg.|>3sigma")
    graph_1d_simple(x,p2,"InjVCal", "Counts" , "b.png",  "S-Curve Untuned Thr.==0")
    graph_1d_simple(x,p3,"InjVCal", "Counts" , "c.png",  "S-Curve Tuned Valued")

    histo_2d_s_curve(p1,201,0,201,51,0,51,"t1.png","InjVCal", "Counts" )
    histo_2d_s_curve(p2,201,0,201,51,0,51,"t2.png","InjVCal", "Counts" )
    histo_2d_s_curve(p3,201,0,201,51,0,51,"t3.png","InjVCal", "Counts" )

    d_n = get_noise_th(point_3d, chip_n, 0)
    graph_1d_simple(np.array(range(len(d_n[0]))),d_n[0], "Dummy", "Noise[e]" , "d.png", "Test")

    # Did the similar plots for the Analog Scan points

    d1 = get_s_curve_all(point_3d_a,s_curve_files, 0)
    d2 = get_s_curve_all(point0_a,s_curve_files, 0)
    d3 = get_s_curve_all(point_n_a, s_curve_files, 0)

    p1,p2,p3 = [],[],[]
    for i in range(10):
        p1.append(d1[np.random.randint( ll[3] )])
        p2.append(d2[np.random.randint( ll[4] )])
        p3.append(d3[np.random.randint( ll[5] )])

    a = np.histogram(d1[0])
    x = np.array(range(len(d1[0])))
    print(d1[0])

    graph_1d_simple(x,p1,"InjVCal", "Counts",  "a_.png" , "S-Curve Analog Occ !=100 && !=0")
    graph_1d_simple(x,p2,"InjVCal", "Counts" , "b_.png",  "S-Curve Analog Occ == 0")
    graph_1d_simple(x,p3,"InjVCal", "Counts" , "c_.png",  "S-Curve Analog Occ == 100")

    histo_2d_s_curve(p1,201,0,201,51,0,51,"al1.png","InjVCal", "Counts" )
    histo_2d_s_curve(p2,201,0,201,51,0,51,"al2.png","InjVCal", "Counts" )
    histo_2d_s_curve(p3,201,0,201,51,0,51,"al3.png","InjVCal", "Counts" )

# range [directly influence the plots]
# May not be necessary
# Might explore later and find the chip with best condition
def th_an_draw(l1, l2, r_1, r_2, s_1, s_2):

    # Did the similar plots for the Analog Scan points
    d1 = get_s_curve_all(l1[0], s_1, 0)
    d2 = get_s_curve_all(l1[1], s_1, 0)
    d3 = get_s_curve_all(l1[2], s_1, 0)

    p1,p2,p3 = [],[],[]
    for i in range(10):
        p1.append(d1[np.random.randint( len(l1[0]) )])
        p2.append(d2[np.random.randint( len(l1[0]) )])
        p3.append(d3[np.random.randint( len(l1[0]) )])

    a = np.histogram(d1[0])
    x = np.array(range(len(d1[0])))
    print(d1[0])

    graph_1d_simple(x,p1,"InjVCal", "Counts",  "a_.png" , "S-Curve Analog Occ !=100 && !=0")
    graph_1d_simple(x,p2,"InjVCal", "Counts" , "b_.png",  "S-Curve Analog Occ == 0")
    graph_1d_simple(x,p3,"InjVCal", "Counts" , "c_.png",  "S-Curve Analog Occ == 100")

    histo_2d_s_curve(p1,201,0,201,51,0,51,"al1.png","InjVCal", "Counts" )
    histo_2d_s_curve(p2,201,0,201,51,0,51,"al2.png","InjVCal", "Counts" )
    histo_2d_s_curve(p3,201,0,201,51,0,51,"al3.png","InjVCal", "Counts" )


def test_12_23():
    # first try 1000e
    threshold_1 = ["/home/shiwen/RD53A/Data/KEKQ13/211210_142/last_scan"]
    analog_1 = ["/home/shiwen/RD53A/Data/KEKQ13/211210_142/055763_diff_analogscan"]
    s_curve_files_1 = readfile("/home/shiwen/RD53A/Data/KEKQ13/211210_142/last_scan/s_curve.txt")

    # 1500e 200 InjVCal 
    threshold = ["/home/shiwen/RD53A/Data/KEKQ13/211210_144/last_scan"]
    s_curve_files = readfile("/home/shiwen/RD53A/Data/KEKQ13/211210_144/last_scan/s_curve.txt")
    analog = ["/home/shiwen/RD53A/Data/KEKQ13/211210_144/055771_diff_analogscan"]


    threshold_1 = ["/home/shiwen/RD53A/Data/KEKQ13/211210_140/last_scan"]
    s_curve_files = readfile("/home/shiwen/RD53A/Data/KEKQ13/211210_140/last_scan/s_curve.txt")
    analog_1 = ["/home/shiwen/RD53A/Data/KEKQ13/211210_142/055763_diff_analogscan"]

    list = sorted(["/home/shiwen/RD53A/Data/KEKQ16/211223_045/056017_diff_thresholdscan",
"/home/shiwen/RD53A/Data/KEKQ16/211223_025/055927_diff_thresholdscan",
"/home/shiwen/RD53A/Data/KEKQ16/211223_047/056028_diff_thresholdscan",
"/home/shiwen/RD53A/Data/KEKQ16/211223_033/055954_diff_thresholdscan",
"/home/shiwen/RD53A/Data/KEKQ16/211223_029/055941_diff_thresholdscan",
"/home/shiwen/RD53A/Data/KEKQ16/211223_038/055977_diff_thresholdscan",
"/home/shiwen/RD53A/Data/KEKQ16/211223_043/056006_diff_thresholdscan",
"/home/shiwen/RD53A/Data/KEKQ16/211223_035/055965_diff_thresholdscan"])
    list_b = sorted([
"/home/shiwen/RD53A/Data/KEKQ16/211223_045/056016_diff_analogscan",
"/home/shiwen/RD53A/Data/KEKQ16/211223_045/056010_diff_analogscan",
"/home/shiwen/RD53A/Data/KEKQ16/211223_025/055920_diff_analogscan",
"/home/shiwen/RD53A/Data/KEKQ16/211223_025/055926_diff_analogscan",
"/home/shiwen/RD53A/Data/KEKQ16/211223_047/056021_diff_analogscan",
"/home/shiwen/RD53A/Data/KEKQ16/211223_047/056027_diff_analogscan",
"/home/shiwen/RD53A/Data/KEKQ16/211223_033/055953_diff_analogscan",
"/home/shiwen/RD53A/Data/KEKQ16/211223_033/055947_diff_analogscan",
"/home/shiwen/RD53A/Data/KEKQ16/211223_029/055934_diff_analogscan",
"/home/shiwen/RD53A/Data/KEKQ16/211223_029/055939_diff_analogscan",
"/home/shiwen/RD53A/Data/KEKQ16/211223_038/055970_diff_analogscan",
"/home/shiwen/RD53A/Data/KEKQ16/211223_038/055976_diff_analogscan",
"/home/shiwen/RD53A/Data/KEKQ16/211223_043/056005_diff_analogscan",
"/home/shiwen/RD53A/Data/KEKQ16/211223_043/055999_diff_analogscan",
"/home/shiwen/RD53A/Data/KEKQ16/211223_035/055964_diff_analogscan",
"/home/shiwen/RD53A/Data/KEKQ16/211223_035/055958_diff_analogscan"
    ])
    fe = [0,0,1,0,1,0,1,1]
    for i,x in enumerate(list):
        t = [x]
        a = [list_b[2*i+1]]
        get_point_list(t, a, fe[i])
    
    #chip_compare(threshold, analog , threshold_1, analog_1)

# Here it gives a general conditions for what
# the chip it self
def get_point_list(threshold, analog, fe):
    print("[ General Condition Change Comparison Analysis]", threshold[0])
    # Combine the threshold and analog to analyze
    chip_c = combine_threshold_file(threshold, chip_threshold_[fe])
    chip_a = combine_threshold_file(analog,  chip_analog_[fe])
    tmap, point_3d, point0, point_n = get_points_s_curve(chip_c,264)
    amap, point_3d_a, point0_a, point_n_a = get_points_s_curve_analog(chip_a, 264)
    tmap_u, avg, std = threshold_occ_stat_u(chip_c,264)

    # Points in Threshold Scan NOT in Analog Scan
    point_3d_e = list(set(point_3d_a)-set(point_3d))
    point0_e = list(set(point0_a)-set(point0))  
    point_n_e  = list(set(point0_a)-set(point0))  

    ll = []
    ll.append(point_3d)
    ll.append(point0)
    ll.append(point_n)
    ll.append(point_3d_a)
    ll.append(point0_a)
    ll.append(point_n_a)
    ll.append(point_3d_e)
    ll.append(point0_e)
    ll.append(point_n_e)
    print("[Debug Threshold] 3std, 0, normal",len(ll[0]),len(ll[1]),len(ll[2]))
    print("[Debug Analog] abnormal, 0, normal",len(ll[3]),len(ll[4]),len(ll[5]))
    print("[Debug] In Th. NOT in Analog 3std, 0, normal", len(ll[6]),len(ll[7]),len(ll[8]))
    print()
    return ll


def chip_compare(t_1, a_1, t_2, a_2): 
    print("[Condition Change Comparison Analysis]")
    l1 = get_point_list(t_1, a_1)
    l2 = get_point_list(t_2, a_2)
    # Three S-Curve Plotter As I know 
    # Parameters:
    # 1 Range 2 Target 3 Chip
    # First do range Comparison
    print("[Scan Range Comparison]: ")

    print("[Targeting Threshold Change]: ")
    print("[Chip difference]: ")

# Threshold Tuning Folder in order of 1000
file_folder_1000 =[
    'KEKQ13/211128_037/054287_diff_thresholdscan/',
    'KEKQ13/211128_037/054292_diff_thresholdscan/',
    'KEKQ13/211128_038/054298_diff_thresholdscan/',
    'KEKQ13/211128_038/054303_diff_thresholdscan/',
    'KEKQ13/211128_038/054308_diff_thresholdscan/',
    'KEKQ13/211128_038/054313_diff_thresholdscan/',
    'KEKQ13/211128_038/054318_diff_thresholdscan/',
    'KEKQ13/211128_038/054323_diff_thresholdscan/',
    'KEKQ13/211128_038/054328_diff_thresholdscan/',
    'KEKQ13/211128_038/054333_diff_thresholdscan/',
    'KEKQ13/211128_038/054338_diff_thresholdscan/'
]

file_folder_1020 =[
    'KEKQ13/211128_039/054343_diff_thresholdscan/',
    'KEKQ13/211128_039/054348_diff_thresholdscan/',
    'KEKQ13/211128_039/054353_diff_thresholdscan/',
    'KEKQ13/211128_039/054358_diff_thresholdscan/',
    'KEKQ13/211128_039/054363_diff_thresholdscan/',
    'KEKQ13/211128_039/054368_diff_thresholdscan/',
    'KEKQ13/211128_039/054373_diff_thresholdscan/',
    'KEKQ13/211128_039/054378_diff_thresholdscan/',
    'KEKQ13/211128_039/054383_diff_thresholdscan/',
    'KEKQ13/211128_039/054388_diff_thresholdscan/',
    'KEKQ13/211128_039/054393_diff_thresholdscan/'
]

file_folder_1040 =[
    'KEKQ13/211128_040/054398_diff_thresholdscan/',
    'KEKQ13/211128_040/054403_diff_thresholdscan/',
    'KEKQ13/211128_040/054408_diff_thresholdscan/',
    'KEKQ13/211128_040/054413_diff_thresholdscan/',
    'KEKQ13/211128_040/054418_diff_thresholdscan/',
    'KEKQ13/211128_040/054423_diff_thresholdscan/',
    'KEKQ13/211128_040/054428_diff_thresholdscan/',
    'KEKQ13/211128_040/054433_diff_thresholdscan/',
    'KEKQ13/211128_040/054438_diff_thresholdscan/',
    'KEKQ13/211128_040/054443_diff_thresholdscan/',
    'KEKQ13/211128_040/054448_diff_thresholdscan/'
]

file_folder_1060 =[
    'KEKQ13/211128_041/054453_diff_thresholdscan/',
    'KEKQ13/211128_041/054458_diff_thresholdscan/',
    'KEKQ13/211128_041/054463_diff_thresholdscan/',
    'KEKQ13/211128_041/054468_diff_thresholdscan/',
    'KEKQ13/211128_041/054473_diff_thresholdscan/',
    'KEKQ13/211128_041/054478_diff_thresholdscan/',
    'KEKQ13/211128_041/054483_diff_thresholdscan/',
    'KEKQ13/211128_041/054488_diff_thresholdscan/',
    'KEKQ13/211128_041/054493_diff_thresholdscan/',
    'KEKQ13/211128_041/054498_diff_thresholdscan/',
    'KEKQ13/211128_041/054503_diff_thresholdscan/'
]

file_folder_1080 =[
    'KEKQ13/211128_042/054508_diff_thresholdscan/',
    'KEKQ13/211128_042/054513_diff_thresholdscan/',
    'KEKQ13/211128_042/054518_diff_thresholdscan/',
    'KEKQ13/211128_042/054523_diff_thresholdscan/',
    'KEKQ13/211128_042/054528_diff_thresholdscan/',
    'KEKQ13/211128_042/054533_diff_thresholdscan/',
    'KEKQ13/211128_042/054538_diff_thresholdscan/',
    'KEKQ13/211128_042/054543_diff_thresholdscan/',
    'KEKQ13/211128_042/054548_diff_thresholdscan/',
    'KEKQ13/211128_042/054553_diff_thresholdscan/',
    'KEKQ13/211128_042/054558_diff_thresholdscan/'
]

file_s = ['KEKQ18/211004_026/049383_std_thresholdscan/',  #1000e
          'KEKQ18/211004_027/049399_std_thresholdscan/',  #1500e
          'KEKQ18/211004_028/049419_std_thresholdscan/',  #2000e
          'KEKQ18/211004_029/049422_std_xrayscan/',
          'KEKQ07/211121_007/052883_diff_thresholdscan/', #1500e 200TCs
          'KEKQ07/211121_007/052884_lin_thresholdscan/',
          'KEKQ07/211121_015/052906_lindiff_noisescan/'
]

file_KEKQ07 = [
        'KEKQ07/211112_048/052511_diff_thresholdscan/', #1500e 100TCs
        'KEKQ07/211112_048/052512_lin_thresholdscan/',
        'KEKQ07/211112_049/052519_lindiff_noisescan/',
        'KEKQ07/211115_001/052525_diff_thresholdscan/', #1500e 125TCs
        'KEKQ07/211115_001/052526_lin_thresholdscan/',
        'KEKQ07/211115_002/052536_lindiff_noisescan/',        
        'KEKQ07/211117_002/052610_diff_thresholdscan/', #1500e 150TCs
        'KEKQ07/211117_002/052611_lin_thresholdscan/',
        'KEKQ07/211117_003/052617_lindiff_noisescan/',
        'KEKQ07/211119_006/052701_diff_thresholdscan/', #1500e 175TCs
        'KEKQ07/211119_006/052702_lin_thresholdscan/',
        'KEKQ07/211119_007/052707_lindiff_noisescan/',
        'KEKQ07/211121_007/052883_diff_thresholdscan/', #1500e 200TCs
        'KEKQ07/211121_007/052884_lin_thresholdscan/',
        'KEKQ07/211121_015/052906_lindiff_noisescan/'
]


all_chips = ['chip1_Occupancy.json','chip2_Occupancy.json','chip3_Occupancy.json','chip4_Occupancy.json']
all_chips_ = ['/chip1_Occupancy.json','/chip2_Occupancy.json','/chip3_Occupancy.json','/chip4_Occupancy.json']

chip_configs_before = ['chip1.json.before','chip2.json.before','chip3.json.before','chip4.json.before']
chip_configs_before_ = ['/chip1.json.before','/chip2.json.before','/chip3.json.before','/chip4.json.before']

chip_threshold = ['chip1_ThresholdMap-0.json','chip2_ThresholdMap-0.json','chip3_ThresholdMap-0.json','chip4_ThresholdMap-0.json']
chip_threshold_ = ['/chip1_ThresholdMap-0.json','/chip2_ThresholdMap-0.json','/chip3_ThresholdMap-0.json','/chip4_ThresholdMap-0.json']

chip_noise = ['chip1_NoiseMap-0.json','chip2_NoiseMap-0.json','chip3_NoiseMap-0.json','chip4_NoiseMap-0.json']
chip_noise_ = ['/chip1_NoiseMap-0.json','/chip2_NoiseMap-0.json','/chip3_NoiseMap-0.json','/chip4_NoiseMap-0.json']

chip_analog = ['chip1_OccupancyMap.json','chip2_OccupancyMap.json','chip3_OccupancyMap.json','chip4_OccupancyMap.json']
chip_analog_ = ['/chip1_OccupancyMap.json','/chip2_OccupancyMap.json','/chip3_OccupancyMap.json','/chip4_OccupancyMap.json']


# The files listed below also inside the ATLAS PC9
# Anyone in ATLAS Japan Group can grab these part of data and 
# take them out 

 
file_folder_800 =[
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_030/054741_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_030/054746_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_030/054751_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_030/054756_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_030/054761_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_030/054766_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_030/054771_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_030/054776_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_030/054781_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_030/054786_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_030/054791_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_030/054796_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_030/054801_diff_thresholdscan"
]

file_folder_820 = [
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_031/054807_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_031/054812_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_031/054817_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_031/054822_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_033/054841_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_033/054846_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_033/054851_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_033/054856_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_033/054861_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_033/054866_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_033/054871_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_033/054876_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_033/054881_diff_thresholdscan"
]

file_folder_840 =sorted([
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_035/054893_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_035/054928_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_035/054923_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_035/054898_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_035/054913_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_035/054908_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_035/054933_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_035/054918_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_035/054943_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_035/054948_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_035/054953_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_035/054938_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_035/054903_diff_thresholdscan"
])

file_folder_860 =sorted([
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_036/054984_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_036/054999_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_036/054959_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_036/055014_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_036/054964_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_036/054989_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_036/055004_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_036/054994_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_036/054979_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_036/055009_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_036/055019_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_036/054974_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_036/054969_diff_thresholdscan"
])

file_folder_880 = sorted([
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_039/055078_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_039/055068_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_039/055038_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_039/055048_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_039/055033_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_039/055063_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_039/055083_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_039/055073_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_039/055043_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_039/055058_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_039/055053_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_040/055090_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_040/055095_diff_thresholdscan"
])

# find /home/shiwen/RD53A/8_Yarr_System/KEKQ13/211204_035 -name "*thresholdscan*" -type d -time

file_folder_1000_repeat = sorted(
    [
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211209_022/055370_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211209_022/055376_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211209_022/055382_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211209_022/055388_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211209_022/055364_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211209_023/055403_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211209_023/055397_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211209_026/055421_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211209_026/055427_diff_thresholdscan",
    "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211209_026/055415_diff_thresholdscan"
    ]
)

file_folder_1000_repeat_Xray = sorted(
    [
        "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211209_035/055471_diff_noisescan",
        "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211209_035/055472_diff_noisescan",
        "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211209_035/055469_diff_noisescan",
        "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211209_035/055470_diff_noisescan",
        "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211209_035/055473_diff_noisescan",
        "/home/shiwen/RD53A/8_Yarr_System/KEKQ13/211209_035/055474_diff_noisescan"
    ]
)

def tdac_all(chips):
    tdac_chip4_KEKQ13, avg_13_4, std_13_4 = tdac_occ(chips, 264)
    histo_1d_simple(tdac_chip4_KEKQ13[0],32,-16,16,"1000e_tdac.png","TDAC", "Counts","1000e")
    histo_1d_simple(tdac_chip4_KEKQ13[25],32,-16,16,"1500e_tdac.png","TDAC", "Counts","1500e")
    histo_1d_simple(tdac_chip4_KEKQ13[35],32,-16,16,"1700e_tdac.png","TDAC", "Counts","1700e")
    histo_1d_simple(tdac_chip4_KEKQ13[50],32,-16,16,"2000e_tdac.png","TDAC", "Counts","2000e")
    map = tdac_repeat(chips, 264) # Repetition Map
    a = []
    a.append(tdac_chip4_KEKQ13[0])
    a.append(tdac_chip4_KEKQ13[25])
    a.append(tdac_chip4_KEKQ13[50])
    histo_1d_multiple(a,32,-16,16,"1000_1500e_2000e_tdac.png","TDAC", "Counts","1000e/1500e")
    pdf_name = "TDAC_Graph.png"

    avg_std_graph(1000,20,avg_13_4, std_13_4, pdf_name, "Threshold [e]", "TDAC")
    histo_2d_signal(tdac_chip4_KEKQ13,55, 1000,2100, 32,-16,16, "t1.png", "Threshold [e]", "tdac" )
    histo_1d_ratio_tdac(tdac_chip4_KEKQ13, 55, 1000,2100, "t2.png", "Threshold","Counts", "tdac")


def threshold_all(chips):
    tmap, avg, std = threshold_occ_stat(chips, 264)
    pdf_name = "Threshold_Graph.png"
    avg_std_graph(1000,20,avg, std, pdf_name, "Threshold Input[e]", "Threshold Tuned [e]")
    histo_1d_ratio_threshold(tmap, 55, 1000,2100, "t3.png", "Threshold","Counts", "Thres")

def tdac_all_(chips):
    tdac_chip4_KEKQ13, avg_13_4, std_13_4 = tdac_occ(chips, 264)
    # histo_1d_simple(tdac_chip4_KEKQ13[0],32,-16,16,"1000e_tdac.png","TDAC", "Counts","1000e")
    # histo_1d_simple(tdac_chip4_KEKQ13[25],32,-16,16,"1500e_tdac.png","TDAC", "Counts","1500e")
    # histo_1d_simple(tdac_chip4_KEKQ13[35],32,-16,16,"1700e_tdac.png","TDAC", "Counts","1700e")
    # histo_1d_simple(tdac_chip4_KEKQ13[50],32,-16,16,"2000e_tdac.png","TDAC", "Counts","2000e")
    # map = tdac_repeat(chips, 264) # Repetition Map
    # a = []
    # a.append(tdac_chip4_KEKQ13[0])
    # a.append(tdac_chip4_KEKQ13[25])
    # a.append(tdac_chip4_KEKQ13[50])
    # histo_1d_multiple(a,32,-16,16,"1000_1500e_2000e_tdac.png","TDAC", "Counts","1000e/1500e")
    pdf_name = "TDAC_Graph_.png"
    avg_std_graph(800,20,avg_13_4, std_13_4, pdf_name, "Threshold [e]", "TDAC")
    histo_2d_signal(tdac_chip4_KEKQ13,65, 800,2100, 32,-16,16, "t1_.png", "Threshold [e]", "+/- 15" )
    histo_1d_ratio_tdac(tdac_chip4_KEKQ13, 65, 800,2100, "t2_.png", "Threshold","Counts", "tdac")


def threshold_tdac_corr(chips_threshold, chips_tdac):
    tdac_chip4_KEKQ13, avg_13_4, std_13_4 = tdac_occ(chips_tdac, 264)
    tmap, avg, std = threshold_occ_stat(chips_threshold, 264)
    histo_1d_signal_tdac_threshold(tdac_chip4_KEKQ13, tmap)
    map1 = map_union(tdac_chip4_KEKQ13, tmap)
    map2 = map_intersection(tdac_chip4_KEKQ13, tmap)
    histo_1d_ratio_threshold(map1, 55, 1000,2100, "t4.png", "Threshold[e]", "Threshold or TDAC", "Union")
    histo_1d_ratio_threshold(map2, 55, 1000,2100, "t5.png", "Threshold[e]", "Threshold&&TDAC", "Inter")


# individual function test case
def test_11_28():
    chip_b = combine_folder_name(file_folder_1000, file_folder_1020, 
        file_folder_1040, file_folder_1060, file_folder_1080)
    chip_c = combine_threshold_file(chip_b, chip_threshold[3])
    chip_d = combine_threshold_file(chip_b, chip_configs_before[3]) 
    threshold_all(chip_c)
    tdac_all(chip_d)
    threshold_tdac_corr(chip_c, chip_d)

def threshold_all_(chips):
    tmap, avg, std = threshold_occ_stat(chips, 264)
    pdf_name = "Threshold_Graph_.png"
    avg_std_graph(800,20,avg, std, pdf_name, "Threshold Input[e]", "Threshold Tuned [e]")
    histo_1d_ratio_threshold(tmap, 65, 800,2100, "t3_.png", "Threshold", "Counts", ">3#sigma")

def noise_all_(chips):
    nmap = noise_occ(chips, 264)
    map1 = nmap[0][0:20, 0:20]
    histo_2d_number(map1,20,20,"Test+","fig_text.png")
    nmap, avg, std = noise_occ_stat(chips, 264)
    avg_std_graph(800,20, avg, std,"Noise_avg_std.png","Threshold Input[e]", "Noise [e]")
    map_repeat = noise_repeat(nmap)

    #plot_threshold_zero(map_repeat)


def test_12_04():
    chip_b = combine_folder_name(file_folder_800, file_folder_820, 
        file_folder_840, file_folder_860, file_folder_880)
    chip_c = combine_threshold_file(chip_b, chip_threshold_[0])
    chip_c = combine_threshold_file(chip_b, chip_threshold_[2])
    chip_d = combine_threshold_file(chip_b, chip_configs_before_[2]) 
    chip_n = combine_threshold_file(chip_b, chip_noise_[2])
    
    noise_all_(chip_n)
    threshold_all_(chip_c)
    tdac_all_(chip_d)
    noise_threshold_corr(chip_c,chip_n)

def threshold_all_repeat(chips):
    tmap, avg, std = threshold_occ_stat(chips, 264)
    nmap, avg, std = noise_occ_stat(chips, 264)
    pdf_name = "Threshold_Repeat.png"
    avg_std_graph(1000,20,avg, std, pdf_name, "Threshold Input[e]", "Threshold Tuned [e]")
    print(len(tmap))
    histo_1d_ratio_threshold_avg_line(nmap, 10,1, len(tmap)+1, "n_r_avg_chip3.png", "Tuned Times", "Counts", "Noi.>3#sigma", "KEKQ13 Chip3 4.6A -100V\n 1000e")
    raw = threshold_repeat(tmap)

# Test the repetition of the scan with 
def test_12_09():
    chip_c = combine_threshold_file(file_folder_1000_repeat, chip_noise_[2])
    threshold_all_repeat(chip_c)
    



# Some threshold Tuning analysis happened on 2021/12/04
def main():
    #test_12_10_()
    test_12_23()


if __name__ == "__main__" :
    main()


# chip_a = combine_string(file_KEKQ07[2], all_chips)
# xray_map = plot_xray_occupancy(chip_a)
