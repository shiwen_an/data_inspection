# Author: Shiwen An
# Date: 2021 Nov. 25th
# Purpose: To find out the important statistical error after threshold
#          Tuning

import json
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from collections import deque
import math
from collections import deque



def get_noise_th(points, chips, start_col):
    list_d = []
    d = []
    for i, x in enumerate(chips):
        with open(x) as json_file:
            occ = json.load(json_file)
        occ_map = occ.get('Data')
        for point in points:
            x1 = point[0]+start_col
            y1 = point[1]
            a = occ_map[x1]
            b = a[y1]
            d.append(b)
        list_d.append(d)
        d = []
    list_d = np.transpose(list_d)
    return list_d

def noise_repeat(noise_maps):
    cnt_ = 0
    raw = noise_maps[0]
    for x2 in noise_maps:
        for i, v in enumerate(x2): 
                for j,x in enumerate(v):
                    if x == 1:
                        raw[i][j] = raw[i][j]+1        

    for i1, x1 in enumerate(raw):
        for j1, y1 in enumerate(x1):
            if  y1 > (len(noise_maps)-5):
                raw[i1][j1] =1
                cnt_ = cnt_+1
            else:
                raw[i1][j1] =0

    print(cnt_)
    plt.imshow(raw, interpolation='none')
    plt.show()
    plt.savefig('noise_repeat.png')
    return raw

# Some simple code for output occupancy maps
def noise_occ(chips, start_col):
    # Find out the problem on Occupancy map
    b2 = []
    d = []
    all_maps = []
    # with open(file_s[2]+chip_config[1]) as json_file:
    for x in chips:
        with open(x) as json_file:
            occ = json.load(json_file)
        occ_map = occ.get('Data')
        for i, v in enumerate(occ_map): 
            if i >=start_col : 
                for x1 in v:
                        d.append(x1)
                b2.append(d)
                d = []
        b2 = np.transpose(b2)
        b2 = np.flip(b2, 0)
        b2 = np.array(b2)
        all_maps.append(b2)
        b2 = []
    return all_maps

# return loose selection without image output
def noise_occ_stat(chips, start_col):
    # Find out the problem on Occupancy map
    b2 = []
    d = []
    avg = []
    std = []
    all_chips = []
    cnt_ = 0
    # with open(file_s[2]+chip_config[1]) as json_file:
    for x in chips:
        with open(x) as json_file:
            occ = json.load(json_file)
        occ_map = occ.get('Data')
        a = get_mean(occ_map,start_col)
        s = get_std(occ_map, start_col,a)
        avg.append(a)
        std.append(s)
        three_sigma_plus = a+3*s
        three_sigma_minus = a-3*s
        for i, v in enumerate(occ_map): 
            if i >=start_col : 
                for i, v in enumerate(v):
                    if v > three_sigma_plus or v<three_sigma_minus: 
                        d.append(1)
                        cnt_ = cnt_ + 1
                    else:
                        d.append(0)
                b2.append(d)
                d = []

        b2 = np.transpose(b2)
        b2 = np.flip(b2, 0)
        all_chips.append(b2)
        print('Threshold Mean: ', a)
        print('Threshold Std: ', s)
        print('Threshold>3sigma: ', cnt_)
        print('Threshold>3sigma Percentage: ', cnt_/26112) # 192*136
        b2 = []
        cnt_ = 0
    return all_chips, avg, std

def get_mean(occ_map, col):
    cnt_ = 0
    avg  = 0
    for i, v in enumerate(occ_map): 
        if i >= col : 
            for i, v in enumerate(v):
                avg =  avg+v
                cnt_ = cnt_+1
    return (avg/cnt_)

def get_std(occ_map, col, avg):
    cnt_ = 0
    std = 0
    for i, v in enumerate(occ_map): 
        if i >= col : 
            for i, v in enumerate(v):
                std = std+(avg-v)*(avg-v)
                cnt_ = cnt_+1
    std =  math.sqrt(std/cnt_)
    return std

