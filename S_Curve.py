# Author: Shiwen AN
# Date: 2021/12/10
# Purpose: Investigate the S-curve
#          for the untuned pixel 
#          Based on different InjVCal value
import json
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from collections import deque
import math
from collections import deque


def get_point_n_xy(tmap):
    a = []
    for i, v in enumerate(tmap):
        for n, l in enumerate(v):
            for j, k in enumerate(l):
                if k ==0:
                    a.append((j,n))
    return a

def get_point_xy(tmap):
    a = []
    for i, v in enumerate(tmap):
        for n, l in enumerate(v):
            for j, k in enumerate(l):
                if k ==1:
                    a.append((j,n))
    return a

def get_s_curve(point, s_curve, start_col):
    d = []
    for x in s_curve:
        with open(x) as json_file:
            occ = json.load(json_file)
        occ_map = occ.get('Data')
        x = point[0]+start_col
        y = point[1]
        a = occ_map[x]
        b = a[y]
        d.append(b)
    return d

def get_s_curve_all(points, s_curve, start_col):
    list_d = []
    d = []
    for i, x in enumerate(s_curve):
        with open(x) as json_file:
            occ = json.load(json_file)
        occ_map = occ.get('Data')
        for point in points:
            x1 = point[0]+start_col
            y1 = point[1]
            a = occ_map[x1]
            b = a[y1]
            d.append(b)
        list_d.append(d)
        d = []
    list_d = np.transpose(list_d)
    return list_d


# return loose selection without image output
# this will be used for one chip only though
def get_points_s_curve(chips, start_col):
    # Find out the problem on Occupancy map
    point0 = []
    point_3std =[]
    point_n=[]
    b2 = []
    d = []
    avg = []
    std = []
    all_chips = []
    cnt_ = 0
    # with open(file_s[2]+chip_config[1]) as json_file:
    for x in chips:
        with open(x) as json_file:
            occ = json.load(json_file)
        occ_map = occ.get('Data')
        a = get_mean(occ_map,start_col)
        s = get_std(occ_map, start_col,a)
        avg.append(a)
        std.append(s)
        three_sigma_plus = a+3*s
        three_sigma_minus = a-3*s
        for i, x in enumerate(occ_map): 
            if i >=start_col : 
                for j, v in enumerate(x):
                    if v == 0:
                        point0.append((i,j))
                    elif v > three_sigma_plus or v<three_sigma_minus: 
                        point_3std.append((i,j))
                    else:
                        point_n.append((i,j))
                b2.append(d)
                d = []

        b2 = np.transpose(b2)
        b2 = np.flip(b2, 0)
        all_chips.append(b2)
        print('Threshold Mean: ', a)
        print('Threshold Std: ', s)
        print('Threshold>3sigma: ', cnt_)
        print('Threshold>3sigma Percentage: ', cnt_/26112) # 192*136
        b2 = []
        cnt_ = 0
    return all_chips, point_3std, point0, point_n

# Analog scan points
def get_points_s_curve_analog(chips, start_col):
    # Find out the problem on Occupancy map
    point0 = []
    point_3std =[]
    point_n=[]
    b2 = []
    d = []
    all_chips = []
    cnt_ = 0
    # with open(file_s[2]+chip_config[1]) as json_file:
    for x in chips:
        with open(x) as json_file:
            occ = json.load(json_file)
        occ_map = occ.get('Data')
        for i, x in enumerate(occ_map):
            if i >=start_col :
                for j, v in enumerate(x):
                    if v == 0:
                        point0.append((i,j))
                    elif v == 100:
                        point_n.append((i,j))
                    else:
                        point_3std.append((i,j))
                b2.append(d)
                d = []
        b2 = np.transpose(b2)
        b2 = np.flip(b2, 0)
        all_chips.append(b2)
        print('Analog OccMap: ', cnt_)
        print('Analog OccMap Per. : ', cnt_/26112) # 192*136
        b2 = []
        cnt_ = 0
    return all_chips, point_3std,point0, point_n


def get_mean(occ_map, col):
    cnt_ = 0
    avg  = 0
    for i, v in enumerate(occ_map): 
        if i >= col : 
            for i, v in enumerate(v):
                avg =  avg+v
                cnt_ = cnt_+1
    return (avg/cnt_)

def get_std(occ_map, col, avg):
    cnt_ = 0
    std = 0
    for i, v in enumerate(occ_map): 
        if i >= col : 
            for i, v in enumerate(v):
                std = std+(avg-v)*(avg-v)
                cnt_ = cnt_+1
    std =  math.sqrt(std/cnt_)
    return std
