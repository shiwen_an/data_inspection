# Author: Shiwen An 
# Date: 2021/12/09 
# Purpose: What Should I do with the analog Scan


import json
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from collections import deque
import math
from collections import deque


# Somehow the analog scan ~ noise scan
def analog_occ(chips, start_col):
    # Find out the problem on Occupancy map
    b2 = []
    d = []
    all_maps = []
    # with open(file_s[2]+chip_config[1]) as json_file:
    for x in chips:
        with open(x) as json_file:
            occ = json.load(json_file)
        occ_map = occ.get('Data')
        for i, v in enumerate(occ_map): 
            if i >=start_col : 
                for x1 in v:
                    d.append(x1)
                b2.append(d)
                d = []
        b2 = np.transpose(b2)
        b2 = np.flip(b2, 0)
        b2 = np.array(b2)
        all_maps.append(b2)
        b2 = []
    return all_maps


# Somehow get the point for zero

def analog_occ_stat_zero(chips, start_col):
    # Find out the problem on Occupancy map
    b2 = []
    d = []
    all_chips = []
    cnt_ = 0
    # with open(file_s[2]+chip_config[1]) as json_file:
    for x in chips:
        with open(x) as json_file:
            occ = json.load(json_file)
        occ_map = occ.get('Data')
        for i, v in enumerate(occ_map): 
            if i >=start_col : 
                for i, v in enumerate(v):
                    if v == 0:
                        d.append(1)
                    else:
                        d.append(0)
                b2.append(d)
                d = []
        b2 = np.transpose(b2)
        b2 = np.flip(b2, 0)
        all_chips.append(b2)
    return all_chips