# Author: Shiwen An 
# Date: 2021/12/12
# Purpse: Trying to read over 200 files name and 
# take data from those plots

from __future__ import print_function
from ROOT import TCanvas, TGraph
from ROOT import gROOT
from math import sin
from array import array
 

def graph_1d_simple(x, y):
    c1 = TCanvas( 'c1', 'A Simple Graph Example', 200, 10, 700, 500 )
    c1.SetFillColor( 42 )
    c1.SetGrid()
    gr = TGraph( len(x), x, y )
    gr.SetLineColor( 2 )
    gr.SetLineWidth( 4 )
    gr.SetMarkerColor( 4 )
    gr.SetMarkerStyle( 21 )
    gr.SetTitle( 'a simple graph' )
    gr.GetXaxis().SetTitle( 'X title' )
    gr.GetYaxis().SetTitle( 'Y title' )
    gr.Draw( 'ACP' )

def readfile(filename):
    f= open(filename, "r")
    a = []
    for x in f: 
        x = x[0: len(x)-1]
        a.append(x)
    a = sorted(a)
    a = sorted(a, key=len)
    print(a)
    return a 


def main():
    filename = "/home/shiwen/RD53A/Data/KEKQ13/211210_142/last_scan/s_curve.txt"
    readfile(filename)


if __name__ == "__main__" :
    main()